﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class AppManager : MonoBehaviour
{
    public static AppManager Instance;
    public GameObject HomePanel;
    public GameObject MainScreen;
    public GameObject MainScreen2;
    public GameObject PickImagePanel;
    public GameObject RecordPanel;
    public GameObject Previewpanel;
    public GameObject RecordProgressPanel;
    public GameObject Audio;
    private Texture2D Texture;
    public Sprite ImageSprite;
    public string SaveVideoPath;
    public RawImage ImagePreview;

    private void Awake()
    {
        Instance = this;
    }

    public void ShowMainScreen()
    {
        // Texture = pickImageManager.Instance.SelectedImage;
        ImagePreview.texture = Texture;
        // ImageSprite = Sprite.Create(Texture, new Rect(0, 0, Texture.width, Texture.height), Vector2.zero);
        // MainScreen.transform.GetChild(0).transform.GetChild(0).GetComponent<Image>().sprite = ImageSprite;
        // PlayPauseAudio(true);
        // MainScreen.SetActive(true);
        // MainScreen2.SetActive(true);
        // PickImagePanel.SetActive(false);
        SceneManager.LoadScene(1);
    }

    
    
    public void OnSaveBtnClick()
    {
        PlayPauseAudio(false);
        RecordPanel.SetActive(true);
        RecordPanel.transform.GetChild(0).GetComponent<Image>().sprite = ImageSprite;
        RecordProgressPanel.SetActive(true);
        MainScreen.SetActive(false);
    }

    
    
    public void ShowPreviewPanel()
    {
        Debug.LogError("AppManager /SaveVideoPath : " + SaveVideoPath);
        RecordPanel.SetActive(false);
        RecordProgressPanel.SetActive(false);
        Previewpanel.SetActive(true);
    }

    
    
    public void ShowHomePanel()
    {
        Previewpanel.SetActive(false);
        //HomePanel.SetActive(true);
        PickImagePanel.SetActive(true);
    }

    
    public void PlayPauseAudio(bool play)
    {
        if(play)
            Audio.GetComponent<AudioSource>().Play();
        else
            Audio.GetComponent<AudioSource>().Stop();
    }
    
}
