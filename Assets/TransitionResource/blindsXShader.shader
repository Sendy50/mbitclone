﻿Shader "gemmine/transition/blindsXShader"
{
    Properties
    {
        _MainTex ("Base (RGB)", 2D) = "white" {}
        _MainTex2 ("Base (RGB)", 2D) = "white" {}
        _Blend ( "Blend", Range ( 0, 1 ) ) = 0
        _NumBlinds ( "Number of Blinds", Range ( 0, 10 ) ) = 10
        _XY ( "Blend", Range ( 0, 1 ) ) = 0
    }
    SubShader
    {
        Tags
        {
            "RenderType"="Opaque"
            "Queue" = "Transparent"
        }
        LOD 200

        CGPROGRAM
        #pragma surface surf Lambert

        sampler2D _MainTex;
        sampler2D _MainTex2;
        float _Blend;
        float _XY;
        float _NumBlinds;

        struct Input
        {
            float2 uv_MainTex;
            float2 uv_MainTex2;
        };

        void surf(Input IN, inout SurfaceOutput o)
        {
            if (frac(_XY < 0.5f ? (IN.uv_MainTex.x * _NumBlinds) : (IN.uv_MainTex.y * _NumBlinds)) < _Blend)
            {
                o.Albedo = tex2D(_MainTex2, IN.uv_MainTex2);
            }
            else
            {
                o.Albedo = tex2D(_MainTex, IN.uv_MainTex);
            }
            o.Alpha = tex2D(_MainTex, IN.uv_MainTex).a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}