﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Experimental.AssetBundlePatching;

public static class Utils 
{
    private static AssetBundle bundle;
    public static string JsonPath = Application.persistentDataPath + "/JsonFlie";
    public static string JsonFileName = "/ParticleJson.json";
    public static string ExternalParticalPrefabPath = Application.persistentDataPath + "/AssetBundles/";
    public static string InternalParticalPrefabPath = Application.streamingAssetsPath + "/AssetBundles/";
    private static int newStatusBarValue;
    const byte m_key = 157;
    
    public static Sprite GetSpritefromImage(string imgPath) {
 
        //Converts desired path into byte array
        byte[] pngBytes = System.IO.File.ReadAllBytes(imgPath);
 
        //Creates texture and loads byte array data to create image
        Texture2D tex = new Texture2D(2, 2);
        tex.LoadImage(pngBytes);
 
        //Creates a new Sprite based on the Texture2D
        Sprite fromTex = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
        return fromTex;
    }
    
    
    

    public static GameObject GetPrefabfromPath(string prefabPath, string prefabName)
    {
        Debug.LogError("GetPrefabfromPath : "+prefabPath+" "+prefabName);
        var myLoadedAssetBundle = AssetBundle.LoadFromFile(prefabPath);
        if (myLoadedAssetBundle == null)
        {
            Debug.Log("Failed to load AssetBundle!");
            return null;
        }

        var prefab = myLoadedAssetBundle.LoadAsset<GameObject>(prefabName);

        myLoadedAssetBundle.Unload(false);
        
        return prefab;
    }
    
    public static IEnumerator GetPrefabfromPath(string prefabPath, string prefabName, Action<GameObject> listener)
    {
        Debug.LogError("GetPrefabfromPath : "+prefabPath+" "+prefabName);
        WWW www = WWW.LoadFromCacheOrDownload(prefabPath, 1);
        yield return www;

        bundle = www.assetBundle;
        Debug.LogError("bundle : "+bundle.name);
        GameObject prefab = bundle.LoadAsset<GameObject>(prefabName);
        listener.Invoke(prefab);
        // return prefab;
    }
    
    public static IEnumerator LoadAssetBundleFromStreamingAssetsCoroutine(string path, string bundleName,  Action<GameObject> handler)
    {
        // Loading asset bundle
        var req = AssetBundle.LoadFromFileAsync(path);
        yield return req;
     
        Assert.IsNotNull(req.assetBundle, "AssetBundleLoader : asset bundle wans't loaded from streaming assets");
        Assert.IsNotNull(handler, "No callback handler");
     
        if (req.assetBundle == null)
        {
            handler(null);
            yield break;
        }
     
        GameObject prefab = bundle.LoadAsset<GameObject>(bundleName);
        
        handler(prefab);
    }



    public static IEnumerator ABC(string path, string bundleName,  Action<GameObject> handler)
    {
        Debug.Log(Application.streamingAssetsPath);

        var bundleLoadRequest = AssetBundle.LoadFromFileAsync(path);
 
        yield return bundleLoadRequest;
 
        if(bundleLoadRequest.assetBundle==null)
        {
            Debug.LogError("Failed to load bundle");
        }
        else
        {
            var assetNames = bundleLoadRequest.assetBundle.GetAllAssetNames();
            foreach(var name in assetNames) Debug.Log(name);
 
            var assetLoadRequest = bundleLoadRequest.assetBundle.LoadAssetAsync<GameObject>("bundleName");
 
            yield return assetLoadRequest;
 
            if(assetLoadRequest.asset==null)
            {
                Debug.LogError("Failed to load asset");
            }
            else
            {
                Debug.Log(assetLoadRequest.asset.name);
 
               var Prefsab = assetLoadRequest.asset as GameObject;
               handler(Prefsab);
            }
 
            bundleLoadRequest.assetBundle.Unload(false);
        }
        
        // public static IEnumerator DownloadAssetBundle()
        // {
        //     yield return StartCoroutine(AssetBundleManager.downloadAssetBundle(url, version));
        //
        //     bundle = AssetBundleManager.getAssetBundle(url, version);
        //
        //     if (bundle != null)
        //         Debug.LogError("Download Success.... ");
        //     else
        //         Debug.LogError("Download error please retry");
        //
        //     GameObject obj = bundle.LoadAsset("ExampleObject") as GameObject;
        //     // Unload the AssetBundles compressed contents to conserve memory
        //     //Debug.Log(obj);
        //     bundle.Unload(false);
        //
        // }
    }
    
    
    private static IEnumerator EncryptionAB(string filepath,string filename)
    {
        // string folderName = "AssetBundles";
        // string filePath = Path.Combine(Application.streamingAssetsPath, folderName);
        // WWW www = new WWW("file:///D:\\fcj\\unity2018\\VuforiaStudy\\BuildAssetBundle\\Window\\myab");
        WWW www = new WWW(filepath);
        yield return www;
        if (www.isDone)
        {
            if (www.error == null)
            {
                byte[] bytes = www.bytes;
                for (int i = 0; i <bytes.Length; i++) // Caesar encryption
                {
                    bytes[i] += 1;
                }
                // File.WriteAllBytes("D:\\fcj\\unity2018\\VuforiaStudy\\BuildAssetBundle\\Window\\myab_Encryption.assetbundle", bytes);
                File.WriteAllBytes(Path.Combine(filepath , filename+".bitly"), bytes);
            }
        }
    }
    
    public static IEnumerator EncyptAssetBundle() {
        yield return "start encryption...";
        Debug.LogError("start encryption...");
        string folderName = "AssetBundles";
        string filePath = Path.Combine(Application.streamingAssetsPath, folderName);
        // Path.Combine(Application.streamingAssetsPath, folderName);
            // / / Traverse all the ab packets in the streamingAssets directory, encrypt one by one
        foreach(var f in new DirectoryInfo(filePath).GetFiles("*", SearchOption.TopDirectoryOnly)) {
            yield return "encrypted file:" + f.FullName;
            
            if (!f.Extension.Equals(""))
            {
                continue;
            }
            
            int index = 0;
            
            int.TryParse(f.FullName.Substring(f.FullName.Length - 1), out index);
            
            Debug.LogError("@@@@@ : "+index);
           
            // if (index > 3)
            // {
            //     f.Delete();
            //     File.Delete(filePath+f.Name);
            //     continue;
            // }

            Debug.LogError("encrypted file:" + f.FullName);
            yield return "encrypted file:" + f.FullName;
            Byte[] temp = File.ReadAllBytes(f.FullName);
            Encrypt(ref temp);
            File.WriteAllBytes(f.FullName+".bitly", temp);
            File.Delete(f.FullName);
        }
        Debug.LogError("encryption completed...");
        // AssetDatabase.Refresh();
        yield return "encryption completed...";
        //Refresh the Project folder
    }

    public static void Encrypt(ref byte[] targetData) {
        //Encryption, XOR with key, same as when decrypting
        int dataLength = targetData.Length;
        for(int i = 0; i < dataLength; ++i) {
            targetData[i] = (byte)(targetData[i] ^ m_key);
        }
    }
    
    public static void Decrypt(ref byte[] targetData) {
        //Encryption, XOR with key, same as when decrypting
        int dataLength = targetData.Length;
        for(int i = 0; i < dataLength; ++i) {
            targetData[i] = (byte)(targetData[i] ^ m_key);
        }
    }
    
    
    public static void setStatusBarValue(int value)
    {
        newStatusBarValue = value;
        using (var unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            using (var activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity"))
            {
                activity.Call("runOnUiThread", new AndroidJavaRunnable(setStatusBarValueInThread));
            }
        }
    }
 
    private static void setStatusBarValueInThread()
    {
        using (var unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            using (var activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity"))
            {
                using (var window = activity.Call<AndroidJavaObject>("getWindow"))
                {
                    window.Call("setFlags", newStatusBarValue, -1);
                }
            }
        }
    }
    
    
}
