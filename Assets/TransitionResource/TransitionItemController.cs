﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TransitionItemController : MonoBehaviour
{

    public RawImage Image;
    public Image LockIcon;
    public Text ThemeNameText;
    public Image Border;
    public Image NewTag;
    public Button SelectButton;
    // public GameObject TransitionScript;
    private TransitionController.TransitionData transitionData;


    void Start()
    {
        SelectButton.onClick.AddListener(OnItemClick);
    }

    private void OnItemClick()
    {
        TransitionController.Instance.OnTransitionItemClick(transitionData.Index);
    }
    

    public void SetUp(TransitionController.TransitionData ttransitionData)
    {
        transitionData = ttransitionData;
        if (ttransitionData.Index == GameManager.Instance.SelectedTransitionIndex)
        {
            Border.gameObject.SetActive(true);
        }
        // Image.texture = GameManager.Instance.SelectedImage;
        LockIcon.gameObject.SetActive(ttransitionData.ThemeIsLock);
        ThemeNameText.text = ttransitionData.ThemeName;
    }
}
