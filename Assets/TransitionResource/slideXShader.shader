﻿Shader "gemmine/transition/slideXShader" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_MainTex2 ("Base (RGB)", 2D) = "white" {}
		_Blend ( "Blend", Range ( 0, 1 ) ) = 0
		_XY ( "Blend", Range ( 0, 1 ) ) = 0
	}
	SubShader {
		Tags { "Queue" = "Transparent" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert

		sampler2D _MainTex;
		sampler2D _MainTex2;
		float _Blend;
		float _XY;

		struct Input {
			float2 uv_MainTex;
			float2 uv_MainTex2;
		};

		void surf (Input IN, inout SurfaceOutput o) {
		    float4 c;
		    float2 amount = float2(_XY < 0.5f ?_Blend :0,_XY > 0.5f ?_Blend :0);
		    float2 uv = IN.uv_MainTex;
		    uv += amount;
		    if (any(saturate(uv)-uv)) {
		    	uv = frac(uv);
		    	c = tex2D(_MainTex2,uv);
		    }
		    else {
		    	c = tex2D(_MainTex,uv);
		    }
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
