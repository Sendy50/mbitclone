﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Theme_Puzzle1 : MonoBehaviour
{

    public RawImage rawImage;
    public GameObject AnimPanel;
    
    // Start is called before the first frame update
    void Start()
    {
       Applytheme();
    }

    private void OnEnable()
    {
        if (SceneManager.GetActiveScene().buildIndex == 1)
            AudioManager.Instance.OnPlayerPlayPause += MusicPlaying;
    }

    private void MusicPlaying(bool play)
    {
        Debug.LogError("music playing on puzzle : "+play);
        if (play)
        {
            AnimPanel.SetActive(true);
            rawImage.gameObject.SetActive(false);
            rawImage.gameObject.SetActive(true);
        }
        else
            AnimPanel.SetActive(false);
        
    }

    private void OnDisable()
    {
        if (SceneManager.GetActiveScene().buildIndex == 1) 
            AudioManager.Instance.OnPlayerPlayPause -= MusicPlaying;
    }
    
    public void Applytheme()
    {
        AnimPanel.SetActive(false);
        rawImage.gameObject.SetActive(false);
        rawImage.gameObject.SetActive(true);
        rawImage.transform.localScale = Vector3.one;
        rawImage.texture = GameManager.Instance.SelectedImages[0];
        AnimPanel.SetActive(true);
        
        Sprite sprite = Sprite.Create(GameManager.Instance.SelectedImages[0],new Rect(0.0f, 0.0f,GameManager.Instance.SelectedImages[0].width,GameManager.Instance.SelectedImages[0].height),new Vector2(0.5f,0.5f),100.0f );
        
        for(int i = 0; i < AnimPanel.transform.childCount; i++)
        {
            AnimPanel.transform.GetChild(i).transform.GetChild(0).GetComponent<Image>().sprite = sprite;
        }
        
    }
    
}
