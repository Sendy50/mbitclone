﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class pickImageManager : MonoBehaviour
{
	
	public static pickImageManager Instance;
	public Texture2D TempTex;

	private void Awake()
	{
		Instance = this;
	}

	
	
	IEnumerator LoadTexture (string url) {
		WWW image = new WWW (url);
		yield return image;
		Texture2D texture = new Texture2D (1, 1);
		image.LoadImageIntoTexture (texture);
		// GameManager.Instance.SelectedImage = texture;
		// GameManager.Instance.SelectedImages = new Texture2D[2];
		// GameManager.Instance.SelectedImages[0] = texture;
		// GameManager.Instance.SelectedImages[1] = TempTex;
		Debug.LogError("Load Texture");
		StartCoroutine(GameManager.Instance.OpenPreviewScreen("pickImageManager LoadTexture"));
		// Debug.Log ("Loaded image size: " + texture.width + "x" + texture.height);
	}

	
	public void PickImage()
	{
		
#if UNITY_EDITOR
		string path = UnityEditor.EditorUtility.OpenFilePanel("Open image","","jpg,png,bmp");
		if (!System.String.IsNullOrEmpty (path))
			StartCoroutine(LoadTexture("file:///" + path));
#else

		// Debug.LogError("PickImageBtn Click.....");
		// NativeGallery.Permission permission = NativeGallery.GetImagesFromGallery((paths) =>
		// {
		// 	
		// 	Debug.Log("Image path: " + paths[0]);
		// 	if (paths != null)
		// 	{
		// 		GameManager.Instance.ImagesUpdate(paths,null,null,false);
		// 	}
		// }, "Select a PNG image", "image/png");

		// Debug.Log("Permission result: " + permission);
#endif
	}

	public void EncryptBundle()
	{
		StartCoroutine(Utils.EncyptAssetBundle());
	}
}
