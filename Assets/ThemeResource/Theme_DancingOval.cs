﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Theme_DancingOval : MonoBehaviour
{
    public RawImage rawImage;
    public Image Animimage;
    
    void Start()
    {
        Applytheme();
    }
    
    public void Applytheme()
    {
        rawImage.transform.localScale = Vector3.one;
        rawImage.texture = GameManager.Instance.SelectedImages[0];
        Sprite sprite = Sprite.Create(GameManager.Instance.SelectedImages[0],new Rect(0.0f, 0.0f,GameManager.Instance.SelectedImages[0].width,GameManager.Instance.SelectedImages[0].height),new Vector2(0.5f,0.5f),100.0f );
        Animimage.sprite = sprite;
    }
}
