﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using MyPlugIn;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PreviewScreenManager : MonoBehaviour
{
    public static PreviewScreenManager Instance;
    // public RawImage UserRawImage;

    public GameObject TransitionObject;
    public GameObject ThemeManager;
    public GameObject ThemeControlPanel;
    public GameObject ThemeControlButton;
    public GameObject TransitionManager;
    public GameObject TransitionControlPanel;
    public GameObject TransitionControlButton;
    public GameObject LoadingScreen;
    public GameObject ParticalPlace;
    public GameObject BackDialog;
    public GameObject CreateDialog;
    public GameObject RewardDialog;
    public GameObject WatermarkDialog;
    public GameObject OverlayButton;
    public Image OverlayImage;
    public Sprite[] OverlaySprites;
    private Action<bool> RewardListener;
    private int OverlayCounter = 0;
    private Action<bool> WaterRewardListener;

    public delegate void PlayerImageChange();

    public event PlayerImageChange OnImageChange;

    private void Awake()
    {
        Instance = this;
    }

    private void OnEnable()
    {
        WaterRewardListener += CreateVideo;
    }

    private void OnDisable()
    {
        WaterRewardListener -= CreateVideo;
    }
    
    

    private void CreateVideo(bool iscomplete)
    {
        if (iscomplete)
        {
            Debug.LogError("Reward success from CreateVideo");
            GameManager.Instance.Watermark = false;
        }
        else
        {
            Debug.LogError("Reward Failed from CreateVideo");
            GameManager.Instance.Watermark = true;
        }

        // AdsManager.Instance.HideBanner();
        AudioManager.Instance.StopAudio();
        SceneManager.LoadScene(2);
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.Escape)  && !BackDialog.activeInHierarchy && !CreateDialog.activeInHierarchy)
        {
            ShowBackDialog(BackDialog);
        }
    }

    public void ShowBackDialog(GameObject Dialog)
    {
        Dialog.SetActive(true);
        Dialog.GetComponent<Animator>().Play("BackDialogAnim");
    }

    public void HideBackDialog()
    {
        StartCoroutine(HideBackDialog(BackDialog));
    }

    private IEnumerator HideBackDialog(GameObject Dialog)
    {
        Dialog.GetComponent<Animator>().Play("BackDialogAnimRev");
        yield return new WaitForSeconds(0.4f);
        Dialog.SetActive(false);
    }

    public void HideCreateDialog()
    {
        // CreateDialog.SetActive(false);
        // AudioManager.Instance.ResumeAudio();
        StartCoroutine(HideCreateDialog(CreateDialog));
    }

    private IEnumerator HideCreateDialog(GameObject Dialog)
    {
        LoadingScreen.SetActive(true);
        yield return new WaitForSeconds(1f);
        AudioManager.Instance.ResumeAudio();
        LoadingScreen.SetActive(false);
        Dialog.SetActive(false);
    }

    // Start is called before the first frame update
    IEnumerator Start()
    {
        ShowLoadingScreen();
        CreateDialog.SetActive(GameManager.Instance.isTamplate);
        GameManager.Instance.isTamplate = false;
        
        
       
        // AudioManager.Instance.PlayPauseAudio(true);
        // SetUserImages(GameManager.Instance.SelectedImage);

        // AdsManager.Instance.ShowBanner();
        Debug.LogError("Selected Image Size : " + GameManager.Instance.SelectedImages.Length);
        if (GameManager.Instance.SelectedImages.Length > 1)
        {
            ThemeControlButton.SetActive(false);
            TransitionControlButton.SetActive(true);
        }
        else
        {
            ThemeControlButton.SetActive(true);
            TransitionControlButton.SetActive(false);
        }


        Debug.LogError("OpenPreview Screen");
        Debug.LogError("SetImage : " + GameManager.Instance.SelectedImages[0].name);
       
        ThemeManager.SetActive(true);
        TransitionManager.SetActive(true);

        yield return new WaitForSeconds(0.2f);
        SetUpAllRelatedData();
        PlugInTest.sendEvent("User At Preview Screen");
        yield return null;
        // SetUserImages();
    }

    

    public void ClearChildren(GameObject abc)
    {
        // Debug.LogError("Clear Children : "+abc.transform.parent.parent);
        foreach (RectTransform child in abc.transform)
        {
            Destroy(child.gameObject);
        }
    }

    
    
    public void SetUpAllRelatedData()
    {
        // ThemeManager.GetComponent<ThemeManager>().enabled = true;
        // TransitionManager.GetComponent<TransitionManager>().enabled = true;
        if (GameManager.Instance.SelectedImages.Length > 1)
        {
            TransitionObject.SetActive(true);
            ThemeControlButton.SetActive(false);
            TransitionControlButton.SetActive(true);
            // ThemeManager.SetActive(false);
            // ThemeManager.SetActive(true);
        }
        else
        {
            TransitionObject.SetActive(false);
            ThemeControlButton.SetActive(true);
            TransitionControlButton.SetActive(false);
            // TransitionManager.SetActive(false);
            // TransitionManager.SetActive(true);
        }

        // SetUserImages(GameManager.Instance.SelectedImages[0]);
        Debug.LogError("sETUP aLL DATA");
        ClearChildren(ParticalPlace);
        // StartCoroutine(LoadBundle("trisul"));
        GameObject ab = Instantiate(GameManager.Instance.SelectedParticalPrefab, ParticalPlace.transform);
        AudioManager.Instance.ResetAudio();
        HideLoadingScreen();
        // AudioManager.Instance.OnPlayerPlayPause(true);
        OnImageChange();
    }

    public IEnumerator LoadBundle(String paths)
    {
        Debug.LogError("partical file name :" + Path.GetFileName(paths));
        Debug.LogError("partical path :" + paths);
        var bundleLoadRequest = AssetBundle.LoadFromFileAsync(paths);
        yield return bundleLoadRequest;

        var myLoadedAssetBundle = bundleLoadRequest.assetBundle;
        if (myLoadedAssetBundle == null)
        {
            Debug.Log("Failed to load AssetBundle!");
            yield break;
        }

        var assetLoadRequest = myLoadedAssetBundle.LoadAssetAsync<GameObject>("trisul");
        yield return assetLoadRequest;

        GameManager.Instance.SelectedParticalPrefab = assetLoadRequest.asset as GameObject;

        myLoadedAssetBundle.Unload(false);

        Debug.LogError("Loaded PrefabName : " + GameManager.Instance.SelectedParticalPrefab.name);
    }


    // public void SetUserImages(Texture2D tex)
    public void SetUserImages(Texture2D tex)
    {
        // UserRawImage.texture = tex;
    }

    public void ExportVideo()
    {
        ShowRemoveWatermarkDialog(WaterRewardListener);
    }

    public void ShowAudioScreen()
    {
#if UNITY_ANDROID
        AudioManager.Instance.StopAudio();
        PlugInTest.OpenAudioSelectScreen();
#endif
    }

    public void ShowHomeScreen()
    {
#if UNITY_ANDROID
        PlugInTest.WatchInterAds(b =>
        {
            AudioManager.Instance.StopAudio();
            StartCoroutine(HideBackDialog(BackDialog));
            PlugInTest.OpenMainScreen();
            SceneManager.LoadScene(0);
        });
#endif
    }

    public void ShowMainScreen()
    {
#if UNITY_ANDROID
        AudioManager.Instance.StopAudio();
        PlugInTest.OpenImageSelectScreen();
#endif
    }

    // public void HideAudioSetting()
    // {
    //     ThemeControlPanel.SetActive(false);
    // }

    public void ShowThemeSetting()
    {
        ThemeControlPanel.SetActive(true);
    }

    public void HideThemeSetting()
    {
        ThemeControlPanel.SetActive(false);
    }

    public void ShowTransitionSetting()
    {
        TransitionControlPanel.SetActive(true);
    }

    public void HideTransitionSetting()
    {
        TransitionControlPanel.SetActive(false);
    }


    public void ShowLoadingScreen()
    {
        LoadingScreen.SetActive(true);
    }

    public void HideLoadingScreen()
    {
        LoadingScreen.SetActive(false);
    }

    public void ShowWatchRewardDialog(Action<bool> watchAds)
    {
        RewardListener = watchAds;
        RewardDialog.SetActive(true);
        RewardDialog.GetComponent<Animator>().Play("BackDialogAnim");
    }

    public void HideWatchRewardDialog(bool showReward)
    {
        StartCoroutine(HideWatchRewardDialog(RewardDialog));

        if (showReward)
            PlugInTest.WatchRewardAds(RewardListener);
    }

    private IEnumerator HideWatchRewardDialog(GameObject Dialog)
    {
        Dialog.GetComponent<Animator>().Play("BackDialogAnimRev");
        yield return new WaitForSeconds(0.4f);
        Dialog.SetActive(false);
    }

    public void ShowRemoveWatermarkDialog(Action<bool> watchAds)
    {
        WaterRewardListener = watchAds;
        WatermarkDialog.SetActive(true);
        WatermarkDialog.GetComponent<Animator>().Play("BackDialogAnim");
    }

    public void HideRemoveWatermarkDialog(bool showReward)
    {
        StartCoroutine(HideWatchRewardDialog(WatermarkDialog));

#if UNITY_EDITOR
        CreateVideo(false);
#else
        if(showReward)
            PlugInTest.WatchRewardAds(WaterRewardListener);
        else
        {
            PlugInTest.WatchInterAds(b =>
            {
                CreateVideo(false);
            });
        }
#endif
    }

    public void OverlayClick()
    {
        OverlayCounter++;
        if (OverlayCounter > 4)
        {
            OverlayCounter = 0;
            OverlayImage.gameObject.SetActive(false);
            OverlayButton.transform.GetChild(3).gameObject.SetActive(false);
        }
        else
        {
            OverlayImage.gameObject.SetActive(true);
            OverlayImage.sprite = OverlaySprites[OverlayCounter];
            Debug.LogError("sprite name : " + OverlayImage.sprite.name);
            OverlayButton.transform.GetChild(3).gameObject.SetActive(true);
            OverlayButton.transform.GetChild(3).transform.GetChild(0).GetComponent<Text>().text =
                OverlayCounter.ToString();
        }

        GameManager.Instance.OverlayIndex = OverlayCounter;
    }
}