﻿Shader "gemmine/transition/paperfoldYShader"
{
    Properties
    {
        _MainTex1 ("Base (RGB)", 2D) = "white" {}
        _MainTex2 ("Base (RGB)", 2D) = "white" {}
        _Blend ( "Blend", Range ( 0, 1 ) ) = 0.5
    }
    SubShader
    {
        Tags
        {
            "Queue" = "Transparent"
        }
        LOD 200

        CGPROGRAM
        #pragma surface surf Lambert

        sampler2D _MainTex1;
        sampler2D _MainTex2;
        float _Blend;

        struct Input
        {
            float2 uv_MainTex1;
            float2 uv_MainTex2;
        };

        void surf(Input IN, inout SurfaceOutput o)
        {
            float _blend = _Blend / 2;
            float right = 1 - _blend;
            if (IN.uv_MainTex1.y > _blend && IN.uv_MainTex1.y < right)
            {
                float2 tuv = float2((IN.uv_MainTex1.x), (IN.uv_MainTex1.y - _blend) / (right - _blend));
                float tx = tuv.y;
                if (tx > 0.5)
                {
                    tx = 1 - tx;
                }
                float top = _blend * tx;
                float bottom = 1 - top;
                if (IN.uv_MainTex1.x >= top && IN.uv_MainTex1.x <= bottom)
                {
                    float ty = lerp(0, 1, (tuv.y - top) / (bottom - top));
                    o.Albedo = tex2D(_MainTex1, float2(tuv.x, ty));
                }
            }
            else
            {
                o.Albedo = tex2D(_MainTex2, IN.uv_MainTex2);
            }
        }
        ENDCG
    }
    FallBack "Diffuse"
}