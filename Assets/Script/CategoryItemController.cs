﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class CategoryItemController : MonoBehaviour
{

    public Image CatIcon;
    public Text CatName;
    public Image Border;
    private ParticalDetailsItem CatData;
    private Sprite sprite;
    private UnityWebRequest uwr;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(OnItemClick);
    }

    private void OnItemClick()
    {
        CategoryController.Instance.OnCategoryItemClick(CatData);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetUp(ParticalDetailsItem catData)
    {
        Debug.LogError("SetUp Cat : "+catData.getParticleCatName() );
        CatData = catData;
        GetSprite(catData);
        CatName.text = catData.getParticleCatName();
    }

    private void GetSprite(ParticalDetailsItem getParticalCatImg)
    {
        Sprite sprite = null;
        if (getParticalCatImg.getParticalCatOnline() == 1)
        {
            String path = Application.persistentDataPath+"/Category/"+getParticalCatImg.ParticalCatImg;
            StartCoroutine(GetSpriteFromPath(path, CatIcon));//Utils.GetSpritefromImage(path);
        }
        else
        {
            String path = Application.streamingAssetsPath+"/Category/"+getParticalCatImg.ParticalCatImg;
            Debug.LogError("PAth : "+path);
            // String path = "jar:file://" + Application.dataPath + "!/assets/Category/"+getParticalCatImg.ParticalCatImg;
            // Debug.LogError("Path2new : "+path);
            
            StartCoroutine(GetSpriteFromPath(path, CatIcon));
        }
        
    }

    private IEnumerator GetSpriteFromPath(string path, Image catIcon)
    {
        using (uwr = UnityWebRequestTexture.GetTexture(path))
        {
            yield return uwr.SendWebRequest();
            if (uwr.isNetworkError || uwr.isHttpError)
            {
                Debug.Log(uwr.error);
            }
            else
            {
                Texture2D tex = DownloadHandlerTexture.GetContent(uwr);
                Sprite fromTex = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
                catIcon.sprite = fromTex;
            }
        }
    }
}
