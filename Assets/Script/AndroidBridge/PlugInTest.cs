﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
// using Unity.UNetWeaver;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

namespace MyPlugIn
{
    public class PlugInTest : MonoBehaviour
    {
        //const string pluginName = "com.sendy.unitytestl.MyPlugIn";
        const string pluginName = "com.inbeat.filepicker.listener.MyPlugIn";
        // const string pluginName = "com.inbeat.filepicker.MyPlugIn";    
        //const string pluginName = "com.sendy.pickimage.MyPlugIn";
        
        static AndroidJavaClass _pluginClass;
        static AndroidJavaObject _pluginInstance;
        private static Action<String> PrefabListener;
        private static Action<bool> WatchAdss;
        private static Action<bool> InterAdss;

        //AllCallBack
        class AndroidImageCallback : AndroidJavaProxy
        {
            public AndroidImageCallback() : base("com.inbeat.filepicker.listener.ImageListener")
            {
            }

            public void onImageUpdated(String[] paths, String musicName, String particleName, bool isTemplate,String tamplateName, String tamplateUserName)
            {
                Debug.LogError("Selected Images Size : " + paths.Length);
                Debug.Log("ENTER callback onSuccess: " + paths[0]);

                if (SceneManager.GetActiveScene().buildIndex == 1)
                    GameManager.Instance.ImagesUpdate(paths, musicName, particleName, isTemplate,tamplateName, tamplateUserName);
                else
                {
                    // InterAdss += (b) =>
                    // {
                        //if(b)
                        Debug.LogError("@@@@@ Tamplate Data isTemplate : " + isTemplate);
                        Debug.LogError("@@@@@ Tamplate Data tamplateName : " + tamplateName);
                        Debug.LogError("@@@@@ Tamplate Data tamplateUserName : " + tamplateUserName);
                        GameManager.Instance.ImagesUpdate(paths, musicName, particleName, isTemplate,tamplateName, tamplateUserName);
                    // };
                    // WatchInterAds(InterAdss);
                }
            }
        }

        
        
        class AndroidAudioCallback : AndroidJavaProxy
        {
            public AndroidAudioCallback() : base("com.inbeat.filepicker.listener.AudioListener")
            {
            }

            public void onAudioUpdated(String paths)
            {
                Debug.LogError("ENTER callback onSuccess: " + paths);
                AudioManager.Instance.MusicUpdate(paths);
            }
        }

        class AndroidDownloadSucessCallback : AndroidJavaProxy
        {
            public AndroidDownloadSucessCallback() : base("com.inbeat.filepicker.listener.DownloadSucess")
            {
            }

            public void onDownloadSuccess(bool success)
            {
                Debug.LogError("@@@ Download All Json callback onSuccess: ");
            }
        }

        class AndroidInitCompleteCallBack : AndroidJavaProxy
        {
            public AndroidInitCompleteCallBack() : base("com.inbeat.filepicker.listener.InitCompleteCallBack")
            {
                
            }

            public void onComplete(bool success, String nativeId)
            {
                Debug.LogError("@@@ Download All Json callback onSuccess: ");
                Debug.LogError("@@@ Download All Json callback onSuccess: nativeId : " + nativeId);
                AdsManager.Instance.nativeAdUnitId = nativeId;
                
                PluginInstance.Call("loginSplashScreen", currentActivity, new AndroidLoginInit());

                
                // WatchInterAds(onComplete =>
                // {
                    // SplashScreenManager.Instance.ShowHomeScreen();
                // });
            }
        }

        
        class AndroidLoginInit : AndroidJavaProxy
        {
            public AndroidLoginInit() : base("com.inbeat.filepicker.listener.SplashListener")
            {
                
            }

            public void onSuccess()
            {
                Debug.LogError("AndroidLoginInit onSuccess: ");
                SplashScreenManager.Instance.ShowHomeScreen();
            }
        }
        
        
        
        class AndroidShareCallback : AndroidJavaProxy
        {
            public AndroidShareCallback() : base("com.inbeat.filepicker.listener.ShareListener")
            {
            }

            public void onHomeCLick(bool Home)
            {
                Debug.LogError("ENTER home callback onSuccess: " + Home);
                // if (Home)
                // {
                RecordManager.Instance.ShowHomePanel();
                // }
                // else
                // {
                //     RecordManager.Instance.ShowPreviewPanel();
                // }
            }
        }

        
        
        class AndroidParticalDownloadCallback : AndroidJavaProxy
        {
            public AndroidParticalDownloadCallback() : base("com.inbeat.filepicker.listener.ParticleDownloadListener")
            {
            }

            // public void onDownloadSucess(Boolean IsFailed, String path)
            public void onDownloadSucess(Boolean IsFailed, String path)
            {
                Debug.LogError("ENTER callback ParticalDownload onSuccess: " + path);
                PrefabListener(path);
            }
        }
        

        
        class AndroidRewardAdsCallback : AndroidJavaProxy
        {
            public AndroidRewardAdsCallback() : base("com.inbeat.filepicker.listener.RewardCallBack")
            {
            }

            public void onFailed()
            {
                Debug.LogError("ENTER callback AndroidRewardAds onFailed: ");
                WatchAdss(false);
            }

            public void onComplete(bool isComplete)
            {
                Debug.LogError("ENTER callback AndroidRewardAds onComplete : " + isComplete);
                WatchAdss(true);
            }
        }

        
        
        class AndroidInterAdsCallback : AndroidJavaProxy
        {
            public AndroidInterAdsCallback() : base("com.inbeat.filepicker.listener.InterCallBack")
            {
            }

            public void adFailed(bool isFailed)
            {
                Debug.LogError("ENTER callback Android Inter onFailed: " + isFailed);
                InterAdss(isFailed);
            }
        }
        
        
        
        //All Instance
        private static AndroidJavaObject currentActivity
        {
            get
            {
                return new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>(
                    "currentActivity");
            }
        }

        public static AndroidJavaClass PluginClass
        {
            get
            {
                if (_pluginClass == null)
                {
                    _pluginClass = new AndroidJavaClass(pluginName);
                }

                return _pluginClass;
            }
        }

        public static AndroidJavaObject PluginInstance
        {
            get
            {
                if (_pluginInstance == null)
                {
                    _pluginInstance = PluginClass.CallStatic<AndroidJavaObject>("getInstance");
                }

                return _pluginInstance;
            }
        }

        //All Methods
        public static void InitAndroidModule()
        {
            PluginInstance.Call("initModule", currentActivity, new AndroidInitCompleteCallBack());
        }
        
        public static void sendEvent(String myLog){
            PluginInstance.Call("sendEvent", myLog);
        }
      
        public static void OpenShare()
        {
            PluginInstance.Call("viewShareApp", currentActivity);
        }
        
        public static void OpenMainScreen()
        {
            PluginInstance.Call("OpenHomeScreen", currentActivity, new AndroidImageCallback());
        }

        public static void OpenImageSelectScreen()
        {
            PluginInstance.Call("OpenImageScreen", currentActivity, new AndroidImageCallback());
        }

        public static void OpenAudioSelectScreen()
        {
            PluginInstance.Call("OpenAudioScreen", currentActivity, new AndroidAudioCallback());
        }

        public static void OpenShareScreen()
        {
            PluginInstance.Call("OpenShareScreen", currentActivity, GameManager.Instance.SaveVideoPath,
                new AndroidShareCallback());
        }
        
        public static void downloadParticle(String particleFileName, String AssestLink, Action<String> listener)
        {
            PrefabListener = listener;
            Debug.LogError("AssestLink : " + AssestLink);
            PluginInstance.Call("downloadParticle", currentActivity, AssestLink, particleFileName,
                new AndroidParticalDownloadCallback());
        }
      
        //Ads Methods
        public static void WatchRewardAds(Action<bool> listener)
        {
            WatchAdss = listener;
            if (!AdsManager.Instance.AdsOn)
            {
                WatchAdss.Invoke(true);
                return;
            }

            PluginInstance.Call("viewReward", currentActivity, new AndroidRewardAdsCallback());
        }

        public static void WatchInterAds(Action<bool> listener)
        {
            Debug.LogError("WatchInterAds : >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            InterAdss = listener;
            if (!AdsManager.Instance.AdsOn)
            {
                InterAdss.Invoke(true);
                return;
            }

            PluginInstance.Call("viewInter", currentActivity, new AndroidInterAdsCallback());
        }
    }
}