﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThemeController : MonoBehaviour
{
    // public GameObject ThemePrefab;
    public GameObject ThemeContent;
    public static ThemeController Instance;
    public ThemeData[] Themes;
    public GameObject[] AllThemePrefab;

    private void Awake()
    {
        Instance = this;
    }


    [Serializable]
    public class ThemeData
    {
        public int Index;
        public String ThemeName;

        public bool ThemeIsLock;
        // public GameObject ThemeList;
        // public Material ThemeMat;
        // public AudioSyncer.Biass bias;
    }

    public void ClearSelection()
    {
        foreach (GameObject child in AllThemePrefab)
        {
            child.GetComponent<ThemeItemController>().Border.gameObject
                .SetActive(false);
        }
    }

    public void OnThemeItemClick(int index)
    {
        if (index == GameManager.Instance.SelectedThemeIndex)
        {
            return;
        }
        //Set Theme to MainRawImage Code Here
        Debug.LogError("Selected Theme : " + Themes[index].ThemeName);
        GameManager.Instance.SelectedThemeIndex = index;
        ClearSelection();
        AllThemePrefab[GameManager.Instance.SelectedThemeIndex].GetComponent<ThemeItemController>().Border.gameObject
            .SetActive(true);
        ThemeManager.Instance.ApplyTheme(index);
        PlayPausePanel.Instance.OnPlayBtnClick();
    }


    // Start is called before the first frame update
    void Start()
    {
        AllThemePrefab = new GameObject[ThemeContent.transform.childCount];
        for (int i = 0; i < ThemeContent.transform.childCount; i++)
        {
            AllThemePrefab[i] = ThemeContent.transform.GetChild(i).gameObject;
            ThemeContent.transform.GetChild(i).GetComponent<ThemeItemController>().SetUp(Themes[i]);
        }

        AllThemePrefab[GameManager.Instance.SelectedThemeIndex].GetComponent<ThemeItemController>().Border.gameObject
            .SetActive(true);
        // if(catData._category == Category.Favorite && Favorite)
        // GameObject catObject = Instantiate(ThemePrefab,ThemeContent.transform).gameObject;
        // catObject.transform.localScale = Vector3.one;
    }
}