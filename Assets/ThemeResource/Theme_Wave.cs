using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

// [ExecuteInEditMode]
[AddComponentMenu("Theme/ThemeWave")]
public class Theme_Wave : MonoBehaviour {

	public RawImage rawImage;
    // public Material mat;
    public string variable;
    public AnimationCurve anm;
    public float Mul=1;
    public float Speed=1;
	// Use this for initialization
	public void Start()
	{
		// rawImage.material.SetFloat("_GrayScale_Fade_1",0f);
		// rawImage.texture = GameManager.Instance.SelectedImages[0];
		Applytheme();
	}
	
	public void Applytheme()
	{
		rawImage.transform.localScale = Vector3.one;
		rawImage.texture = GameManager.Instance.SelectedImages[0];
	}
	
	// Update is called once per frame
	void Update ()
    {
	    // Debug.LogError("SceneManager.GetActiveScene().buildIndex : "+SceneManager.GetActiveScene().buildIndex);
	    if ((SceneManager.GetActiveScene().buildIndex == 1)? AudioManager.Instance.isPlaying : RecordManager.Instance.Audio.isPlaying) 
	    {
		    rawImage.material.SetFloat("_Effect_Fade",0.3f);
		    if (rawImage.material!=null) rawImage.material.SetFloat(variable, anm.Evaluate(Time.time * Speed)*Mul);
	    }
	    else
	    {
		    rawImage.material.SetFloat("_Effect_Fade",0);
	    }
    }
}
