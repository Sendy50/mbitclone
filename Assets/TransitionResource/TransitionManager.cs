﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;

public class TransitionManager : MonoBehaviour
{
    public GameObject fadeTransition;
    public GameObject blindXYTransition;
    public GameObject rippleTransition;
    public GameObject slindXYTransition;
    public GameObject radialBlurTransition;
    public GameObject paperFoldTransition;
    public GameObject angle1Transition;
    public GameObject angle2Transition;
    public GameObject angle3Transition;
    public GameObject angle4Transition;
    public GameObject angle5Transition;
    public GameObject angle6Transition;
    public GameObject angle7Transition;
    public static TransitionManager Instance;

    private void Awake()
    {
        Instance = this;
    }

    private void OnEnable()
    {
        PreviewScreenManager.Instance.OnImageChange += OnImageChange;
    }

    private void OnDisable()
    {
        PreviewScreenManager.Instance.OnImageChange -= OnImageChange;
    }

    private void OnImageChange()
    {
        Debug.LogError("OnImageChange : TransitionManager  >>");
        OffAllEffect();
        Start();
    }

    private void Start()
    {
        // OffAllEffect();
        Debug.LogError("TransitionManager Start : "+GameManager.Instance.SelectedImages.Length);
        if (GameManager.Instance.SelectedImages.Length > 1)
        {
            ApplyTheme(GameManager.Instance.SelectedTransitionIndex);
        }
    }

    public void OffAllEffect()
    {
        fadeTransition.SetActive(false);
        blindXYTransition.SetActive(false);
        rippleTransition.SetActive(false);
        slindXYTransition.SetActive(false);
        radialBlurTransition.SetActive(false);
        paperFoldTransition.SetActive(false);
        angle1Transition.SetActive(false);
        angle2Transition.SetActive(false);
        angle3Transition.SetActive(false);
        angle4Transition.SetActive(false);
        angle5Transition.SetActive(false);
        angle6Transition.SetActive(false);
        angle7Transition.SetActive(false);
    }

    public void ApplyTheme(int Index)
    {
        // Debug.LogError("Apply theme1");
        // GetComponent<RawImage>().transform.localScale = Vector3.one;
        OffAllEffect();
        Debug.LogError("Apply Transition : "+Index);
        // Debug.LogError("Apply theme2");
        switch (Index)
        {
            case 0:
                fadeTransition.SetActive(true);
                // fadeTransition.transform.GetChild(0).GetComponent<Transition_Fade>().Applytheme();
                break;
            case 1:
                blindXYTransition.SetActive(true);
                // blindXYTransition.transform.GetChild(0).GetComponent<Transition_BlindXY>().Applytheme();
                break;
            case 2:
                rippleTransition.SetActive(true);
                // rippleTransition.GetComponent<Transition_Ripple>().Applytheme();
                break;
            case 3:
                slindXYTransition.SetActive(true);
                // TwistTheme.Applytheme();
                break;
            case 4:
                radialBlurTransition.SetActive(true);
                // WaveTheme.Applytheme();
                break;
            case 5:
                paperFoldTransition.SetActive(true);
                // WaveTheme.Applytheme();
                break;
            case 6:
                angle1Transition.SetActive(true);
                // WaveTheme.Applytheme();
                break;
            case 7:
                angle2Transition.SetActive(true);
                // WaveTheme.Applytheme();
                break;
            case 8:
                angle3Transition.SetActive(true);
                // WaveTheme.Applytheme();
                break;
            case 9:
                angle4Transition.SetActive(true);
                // WaveTheme.Applytheme();
                break;
            case 10:
                angle5Transition.SetActive(true);
                // WaveTheme.Applytheme();
                break;
            case 11:
                angle6Transition.SetActive(true);
                // WaveTheme.Applytheme();
                break;
            case 12:
                angle7Transition.SetActive(true);
                // WaveTheme.Applytheme();
                break;
        }
        
        AudioManager.Instance.ResetAudio();
        // Debug.LogError("Apply theme3");
    }
}