﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticalMovementController : AudioSyncer
{
    private void Start()
    {
        AudioManager.Instance.OnPlayerPlayPause += MusicPlaying;
    }
    
    private void OnDisable()
    {
        AudioManager.Instance.OnPlayerPlayPause -= MusicPlaying;
    }

    private void MusicPlaying(bool play)
    {
        if (play)
        {
            GetComponent<ParticleSystem>().Play();
            // Debug.LogError("ParticalsPlay");
        }
        else
        {
            GetComponent<ParticleSystem>().Pause();
            // Debug.LogError("ParticalsStop");
        }
    }


    public override void OnBeat()
    {
        base.OnBeat();
        StartCoroutine(BeatParticals());
    }

    private IEnumerator BeatParticals()
    {
        // GetComponent<ParticleSystem>().main.simulationSpeed = 10f;
        var main = GetComponent<ParticleSystem>().main;
        // Debug.LogError("BeatParticals1 : playbackSpeed : "+   main.simulationSpeed);
        main.simulationSpeed = 10f;
        yield return new WaitForSeconds(0.2f);
        main.simulationSpeed = 1f;
        // Debug.LogError("BeatParticals2 : playbackSpeed : "+   main.simulationSpeed);
        
    }
    
    
}
