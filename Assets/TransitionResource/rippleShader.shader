﻿Shader "gemmine/transition/rippleShader"
{
    Properties
    {
        _MainTex ("Base (RGB)", 2D) = "white" {}
        _MainTex2 ("Base (RGB)", 2D) = "white" {}
        _Blend ( "Blend", Range ( 0, 1 ) ) = 0
    }
    SubShader
    {
        Tags
        {
            "RenderType"="Opaque"
            "Queue" = "Transparent"
        }
        LOD 200

        CGPROGRAM
        #pragma surface surf Lambert

        sampler2D _MainTex;
        sampler2D _MainTex2;
        float _Blend;

        struct Input
        {
            float2 uv_MainTex;
            float2 uv_MainTex2;
        };

        void surf(Input IN, inout SurfaceOutput o)
        {
            float frequency = 20;
            float speed = 10;
            float amplitude = 0.05;
            float2 center = float2(0.5, 0.5);

            float2 uv = IN.uv_MainTex;
            float2 toUV = uv - center;
            float distanceFromCenter = length(toUV);
            float2 normToUV = toUV / distanceFromCenter;
            float wave = cos(frequency * distanceFromCenter - speed * _Blend);
            float offset1 = _Blend * wave * amplitude;
            float offset2 = (1.0 - _Blend) * wave * amplitude;
            float2 newUV1 = center + normToUV * (distanceFromCenter + offset1);
            float2 newUV2 = center + normToUV * (distanceFromCenter + offset2);
            float4 c1 = tex2D(_MainTex, newUV1);
            float4 c2 = tex2D(_MainTex2, newUV2);

            o.Albedo = lerp(c1, c2, _Blend);
            o.Alpha = c1.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}