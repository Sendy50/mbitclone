﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubCategoryController : MonoBehaviour
{
    public GameObject ParticalParent;
    public GameObject SubCategoryPrefab;
    public GameObject SubCategoryContent;
    // public SubCatData[] SubCatDataList;
    public static SubCategoryController Instance;
    private List<GameObject> AllSubCatItem;
    

    
    
    private void Awake()
    {
        Instance = this;
    }
    
    
    
    // [Serializable]
    // public class SubCatData
    // {
    //     public CategoryController.Category _category;
    //     public ParticlePrefab[] SubCatList;
    // } 
    
    
    
    public void ShowSubCategory(ParticalDetailsItem catDataCategory)
    {

        Debug.LogError("ShowSubCategory : "+catDataCategory);
        Debug.LogError("ShowSubCategory : "+catDataCategory.getParticalInfo().Count);
        Debug.LogError("ShowSubCategory : "+catDataCategory.getParticalInfo()[0].getThemeName());

        AllSubCatItem = new List<GameObject>();
        
        foreach (ParticalInfoItem subCatData in catDataCategory.getParticalInfo())
        {
            //Debug.LogError("SetupAndVisibleSubItem : "+subCatList.CatName);
            GameObject catObject = Instantiate(SubCategoryPrefab, SubCategoryContent.transform).gameObject;
            catObject.transform.localScale = Vector3.one;
            catObject.GetComponent<SubCategoryItemController>().SetUp(subCatData);
            AllSubCatItem.Add(catObject);
            //Debug.LogError("SetupAndVisibleSubItem parent : "+catObject.transform.parent);
        }
        
    }

    // public void SetupAndVisibleSubItem(ParticlePrefab[] subCatLists)
    // {
    //     // ClearChildren(SubCategoryContent);
    //     AllSubCatItem = new List<GameObject>();
    //     foreach (ParticlePrefab subCatList in subCatLists)
    //     {
    //         // Debug.LogError("SetupAndVisibleSubItem : "+subCatList.CatName);
    //         GameObject catObject = Instantiate(SubCategoryPrefab,SubCategoryContent.transform).gameObject;
    //         catObject.transform.localScale = Vector3.one;
    //         catObject.GetComponent<SubCategoryItemController>().SetUp(subCatList);
    //         AllSubCatItem.Add(catObject);
    //         // Debug.LogError("SetupAndVisibleSubItem parent : "+catObject.transform.parent);
    //     }
    // }
    
    public void ClearChildren(GameObject abc)
    {
        // Debug.LogError("Clear Children : "+abc.transform.parent.parent);
        foreach (RectTransform child in abc.transform)
        {
            Destroy(child.gameObject);    
        }
    }

    public void ClearSelection()
    {
        foreach (GameObject child in AllSubCatItem)
        {
           child.transform.GetChild(0).gameObject.SetActive(false);
        }
    }


    public void OnSubCategoryItemClick(GameObject ParticalPrefab)
    {
        ClearChildren(ParticalParent);
        ClearSelection();
        Instantiate(ParticalPrefab,ParticalParent.transform);
        GameManager.Instance.SelectedParticalPrefab = ParticalPrefab;
    }
}
