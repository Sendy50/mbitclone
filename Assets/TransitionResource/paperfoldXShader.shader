﻿Shader "gemmine/transition/paperfoldXShader"
{
    Properties
    {
        _MainTex ("Base (RGB)", 2D) = "white" {}
        _MainTex2 ("Base (RGB)", 2D) = "white" {}
        _Blend ( "Blend", Range ( 0, 1 ) ) = 0
        _XY ( "Blend", Range ( 0, 1 ) ) = 0
    }
    SubShader
    {
        Tags
        {
            "Queue" = "Transparent"
        }
        LOD 200

        CGPROGRAM
        #pragma surface surf Lambert

        sampler2D _MainTex;
        sampler2D _MainTex2;
        float _Blend;
        float _XY;

        struct Input
        {
            float2 uv_MainTex;
            float2 uv_MainTex2;
        };

        void surf(Input IN, inout SurfaceOutput o)
        {
            if (_XY < 0.5f)
            {
                float _blend = _Blend / 2;
                float right = 1 - _blend;
                if (IN.uv_MainTex.x > _blend && IN.uv_MainTex.x < right)
                {
                    float2 tuv = float2((IN.uv_MainTex.x - _blend) / (right - _blend), IN.uv_MainTex.y);
                    float tx = tuv.x;
                    if (tx > 0.5)
                    {
                        tx = 1 - tx;
                    }
                    float top = _blend * tx;
                    float bottom = 1 - top;
                    if (IN.uv_MainTex.y >= top && IN.uv_MainTex.y <= bottom)
                    {
                        float ty = lerp(0, 1, (tuv.y - top) / (bottom - top));
                        o.Albedo = tex2D(_MainTex, float2(tuv.x, ty));
                    }
                }
                else
                {
                    o.Albedo = tex2D(_MainTex2, IN.uv_MainTex2);
                }
            }
            else
            {
                float _blend = _Blend / 2;
                float right = 1 - _blend;
                if (IN.uv_MainTex.y > _blend && IN.uv_MainTex.y < right)
                {
                    float2 tuv = float2((IN.uv_MainTex.x), (IN.uv_MainTex.y - _blend) / (right - _blend));
                    float tx = tuv.y;
                    if (tx > 0.5)
                    {
                        tx = 1 - tx;
                    }
                    float top = _blend * tx;
                    float bottom = 1 - top;
                    if (IN.uv_MainTex.x >= top && IN.uv_MainTex.x <= bottom)
                    {
                        float ty = lerp(0, 1, (tuv.y - top) / (bottom - top));
                        o.Albedo = tex2D(_MainTex, float2(tuv.x, ty));
                    }
                }
                else
                {
                    o.Albedo = tex2D(_MainTex2, IN.uv_MainTex2);
                }
            }
        }
        ENDCG
    }
    FallBack "Diffuse"
}