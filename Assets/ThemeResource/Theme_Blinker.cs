
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// [ExecuteInEditMode]
[AddComponentMenu("Theme/Blinker")]
public class Theme_Blinker : AudioSyncer
{
    public bool prefab;
    public RawImage rawImage;

    public void Start()
    {
        rawImage.material.SetFloat("_GrayScale_Fade_1",0f);
        Applytheme();
    }

    public void Applytheme()
    {
        Debug.LogError("Settexture : "+GameManager.Instance.SelectedImages[0].name);
        rawImage.transform.localScale = Vector3.one;
        rawImage.texture = GameManager.Instance.SelectedImages[0];
    }
    
    
   
    
    private IEnumerator DoDark()
    {
        
        // gameObject.GetComponent<Renderer>().sharedMaterial.SetFloat("_YourParameter", someValue);
        rawImage.material.SetFloat("_GrayScale_Fade_1",0f);
       
        yield return new WaitForSeconds(0.1f);
        // OnBeatt = false;
        m_isBeat = false;
    }
    
  
    
    public override void OnUpdate()
    {
        base.OnUpdate();

        if (m_isBeat) return;
        rawImage.material.SetFloat("_GrayScale_Fade_1",1f);
    }
    
    public override void OnBeat()
    {
        base.OnBeat();
        StopCoroutine(DoDark());
        StartCoroutine(DoDark());
        // OnBeatt = true;
    }
    
}