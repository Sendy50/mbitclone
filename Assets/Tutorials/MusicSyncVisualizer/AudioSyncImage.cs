﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class AudioSyncImage : AudioSyncer {

	public Sprite beatSprites;
	public Sprite restSprite;

	private int m_randomIndx;
	private Image m_img;

	private IEnumerator MoveToSprite(Sprite _target)
	{
		Sprite _curr = m_img.sprite;
		Sprite _initial = _curr;
		float _timer = 0;
		
		while (_curr != _target)
		{
			_curr = _target;
			_timer += Time.deltaTime;

			m_img.sprite = _curr;

			yield return null;
		}

		m_isBeat = false;
	}
	

	public override void OnUpdate()
	{
		base.OnUpdate();

		if (m_isBeat) return;

		m_img.sprite = restSprite;
	}

	public override void OnBeat()
	{
		base.OnBeat();
		
		StopCoroutine("MoveToSprite");
		StartCoroutine("MoveToSprite", beatSprites);
		//print("OnBeat : " + _c);
	}

	private void Start()
	{
		m_img = GetComponent<Image>();
	}
}
