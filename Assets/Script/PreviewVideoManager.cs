﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class PreviewVideoManager : MonoBehaviour
{
    public RawImage VideoTexure;

    public void Start()
    {
       
       

        Debug.LogError("PreviewVideoManager Start : " + "file://" + AppManager.Instance.SaveVideoPath);
        VideoTexure.GetComponent<VideoPlayer>().url = "file://"+ AppManager.Instance.SaveVideoPath;
        VideoTexure.GetComponent<VideoPlayer>().Play();
    }
}
