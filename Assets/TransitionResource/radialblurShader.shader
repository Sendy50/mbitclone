﻿Shader "gemmine/transition/radialblurShader" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_MainTex2 ("Base (RGB)", 2D) = "white" {}
		_Blend ( "Blend", Range ( 0, 1 ) ) = 0
	}
	SubShader {
		Tags {  "Queue" = "Transparent" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert
		#pragma  exclude_renderers flash

		sampler2D _MainTex;
		sampler2D _MainTex2;
		float _Blend;

		struct Input {
			float2 uv_MainTex;
			float2 uv_MainTex2;
		};

		void surf (Input IN, inout SurfaceOutput o) {
  			float2 center = float2(0.5,0.5);
  			float2 toUV = IN.uv_MainTex2 - center;
  			float2 normToUV = toUV;

  			float4 c1 = float4(0,0,0,0);
  			int count = 8;
  			float s = _Blend * 0.06;

			for(int i=0; i<count; i++)
			{
				c1 += tex2D(_MainTex, IN.uv_MainTex - normToUV * s * i);
			}

  			c1 /= count;
    		float4 c2 = tex2D(_MainTex2, IN.uv_MainTex2);

			o.Albedo = lerp(c1, c2, _Blend);
		  	o.Alpha = c1.a;			
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
