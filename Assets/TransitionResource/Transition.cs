﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Transition : MonoBehaviour
{
    public static Transition Instance;

    public MeshRenderer[] MainImage;
    public RawImage[] Items;

    private void Awake()
    {
        Instance = this;
    }

    public void UpdateAllTrasition()
    {
        if (GameManager.Instance.SelectedImages.Length == 1)
        {
            return;
        }

        transform.GetChild(0).GetComponent<Transition_Fade>().Applytheme();
        transform.GetChild(1).GetComponent<Transition_BlindXY>().Applytheme();
        transform.GetChild(2).GetComponent<Transition_Ripple>().Applytheme();
        transform.GetChild(3).GetComponent<Transition_SlideXY>().Applytheme();
        transform.GetChild(4).GetComponent<Transition_RadialBlur>().Applytheme();
        transform.GetChild(5).GetComponent<Transition_paperFold>().Applytheme();
        transform.GetChild(6).GetComponent<Transition_angle1>().Applytheme();
        transform.GetChild(7).GetComponent<Transition_angle1>().Applytheme();
        transform.GetChild(8).GetComponent<Transition_angle1>().Applytheme();
        transform.GetChild(9).GetComponent<Transition_angle1>().Applytheme();

        MainImage[0].material = Items[0].material = transform.GetChild(0).GetComponent<MeshRenderer>().sharedMaterial;
        MainImage[1].material = Items[1].material = transform.GetChild(1).GetComponent<MeshRenderer>().sharedMaterial;
        MainImage[2].material = Items[2].material = transform.GetChild(2).GetComponent<MeshRenderer>().sharedMaterial;
        MainImage[3].material = Items[3].material = transform.GetChild(3).GetComponent<MeshRenderer>().sharedMaterial;
        MainImage[4].material = Items[4].material = transform.GetChild(4).GetComponent<MeshRenderer>().sharedMaterial;
        MainImage[5].material = Items[5].material = transform.GetChild(5).GetComponent<MeshRenderer>().sharedMaterial;
        MainImage[6].material = Items[6].material = transform.GetChild(6).GetComponent<MeshRenderer>().sharedMaterial;
        MainImage[7].material = Items[7].material = transform.GetChild(7).GetComponent<MeshRenderer>().sharedMaterial;
        MainImage[8].material = Items[8].material = transform.GetChild(8).GetComponent<MeshRenderer>().sharedMaterial;
        MainImage[9].material = Items[9].material = transform.GetChild(9).GetComponent<MeshRenderer>().sharedMaterial;

        for (int i = 0; i < transform.childCount; i++)
        {
            MainImage[i].material = transform.GetChild(i).GetComponent<MeshRenderer>().sharedMaterial;
            if (SceneManager.GetActiveScene().buildIndex == 1)
                Items[i].material = transform.GetChild(i).GetComponent<MeshRenderer>().sharedMaterial;
        }

        if (SceneManager.GetActiveScene().buildIndex != 1)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                if (i == GameManager.Instance.SelectedTransitionIndex)
                {
                    transform.GetChild(i).gameObject.SetActive(true);
                }
                else
                {
                    transform.GetChild(i).gameObject.SetActive(false);
                }
            }
        }
    }
}