﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ThemeItemController : MonoBehaviour
{

    public RawImage Image;
    public Image LockIcon;
    public Text ThemeNameText;
    public Image Border;
    public Image NewTag;
    public Button SelectButton;
    private ThemeController.ThemeData Themedata;


    void Start()
    {
        SelectButton.onClick.AddListener(OnItemClick);
    }

    private void OnItemClick()
    {
        ThemeController.Instance.OnThemeItemClick(Themedata.Index);
    }
    

    public void SetUp(ThemeController.ThemeData themeData)
    {
        Themedata = themeData;
        Image.texture = GameManager.Instance.SelectedImages[0];
        LockIcon.gameObject.SetActive(themeData.ThemeIsLock);
        ThemeNameText.text = themeData.ThemeName;
    }
}
