using System;

[Serializable]
public class ParticalInfoItem
{
    public String ImgPath;

    public String BundlePath;

    public String NewFlag;

    public int ParticalOnline;

    public String UniqueIDNo;

    public String ThemeName;

    public String prefbName;
    
    public String AssestLink;

    public int isPremium;

    public void setImgPath(String imgPath)
    {
        this.ImgPath = imgPath;
    }

    public String getImgPath()
    {
        return ImgPath;
    }
 public void setAssestLink(String imgPath)
    {
        this.AssestLink = imgPath;
    }

    public String getAssestLink()
    {
        return AssestLink;
    }

    public void setBundlePath(String bundlePath)
    {
        this.BundlePath = bundlePath;
    }

    public String getBundlePath()
    {
        return BundlePath;
    }


    public void setNewFlag(String newFlag)
    {
        this.NewFlag = newFlag;
    }

    public String getNewFlag()
    {
        return NewFlag;
    }

    public void setParticalOnline(int particalOnline)
    {
        this.ParticalOnline = particalOnline;
    }

    public int getParticalOnline()
    {
        return ParticalOnline;
    }

    public void setUniqueIDNo(String uniqueIDNo)
    {
        this.UniqueIDNo = uniqueIDNo;
    }

    public String getUniqueIDNo()
    {
        return UniqueIDNo;
    }

    public void setThemeName(String themeName)
    {
        this.ThemeName = themeName;
    }

    public String getThemeName()
    {
        return ThemeName;
    }

    public void setPrefbName(String prefbName)
    {
        this.prefbName = prefbName;
    }

    public String getPrefbName()
    {
        return prefbName;
    }

    public void setisPremium(int prefbName)
    {
        this.isPremium = prefbName;
    }

    public int getisPremium()
    {
        return isPremium;
    }


    public String toString()
    {
        return
            "ParticalInfoItem{" +
            "imgPath = '" + ImgPath + '\'' +
            ",bundlePath = '" + BundlePath + '\'' +
            ",newFlag = '" + NewFlag + '\'' +
            ",particalOnline = '" + ParticalOnline + '\'' +
            ",uniqueIDNo = '" + UniqueIDNo + '\'' +
            ",themeName = '" + ThemeName + '\'' +
            ",prefbName = '" + prefbName + '\'' +
            "}";
    }
}