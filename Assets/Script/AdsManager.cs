﻿using System;
using System.Collections;
using System.Collections.Generic;
using GoogleMobileAds.Api;
using MyPlugIn;
using UnityEngine;

public class AdsManager : MonoBehaviour
{
    public static AdsManager Instance;
    [HideInInspector]
    public String bannerAdUnitId = "ca-app-pub-3940256099942544/6300978111";
    [HideInInspector]
    public String nativeAdUnitId = "ca-app-pub-3940256099942544/2247696110";
    private BannerView bannerView;
    private static Action<bool> InterAdss;
    public bool AdsOn = true;


    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);
    }
    
    public void Start()
    {
        InterAdss += onInterCallback;
        // Initialize the Google Mobile Ads SDK.
        // MobileAds.Initialize(initStatus => { });
        // this.RequestBanner();
    }

    private void onInterCallback(bool obj)
    {
       
    }

    public void ShowBanner()
    {
        if (!AdsOn)
        {
            return;    
        }
        
        // Create a 320x50 banner at the top of the screen.
        bannerView = new BannerView(bannerAdUnitId, AdSize.Banner, AdPosition.Bottom);

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();

        // Load the banner with the request.
        bannerView.LoadAd(request);

        Debug.LogError("Show Banner : ====================================================>SSS");
    }

    public void HideBanner()
    {
        if (!AdsOn)
        {
            return;    
        }
        bannerView.Hide();
        bannerView.Destroy();
    }

    public void ShoInterAds(Action<bool> isShow)
    {
        if (!AdsOn)
        {
            return;    
        }
        // PlugInTest.WatchInterAds(isShow);
    }
    
}
