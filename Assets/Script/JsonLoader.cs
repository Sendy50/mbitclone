﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class JsonLoader : MonoBehaviour
{
    public static JsonLoader Instance;
    public ParticalData ParticalsData;
    
    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        LoadGameData();
    }
    
    private void LoadGameData()
    {
        string filePath = Application.streamingAssetsPath + "/defaultParticleJson.json";
        
        Debug.LogError("Filepath : "+filePath);
        
        if (File.Exists(filePath))
        {
            string dataAsJson = File.ReadAllText(filePath);
            ParticalsData = JsonUtility.FromJson<ParticalData>(dataAsJson);
            GameManager.Instance.particleDeta = ParticalsData;
            Debug.LogError("loaded game data : "+ParticalsData.toString());
        }
        else
        {
            Debug.LogError("Cannot load game data");
        }
        
        
        
    }
    
    // public void SaveToString()
    // {
    //     return JsonUtility.ToJson(ParticalsData);
    // }
    
    

    
    
}
