﻿using System;
using System.Collections;
using System.Collections.Generic;
using MyPlugIn;
using UnityEngine;

public class SplashScreenManager : MonoBehaviour
{
    public GameObject LoadngPanel;
    public static SplashScreenManager Instance;

    private void Awake()
    {
        Instance = this;
    }


    // Start is called before the first frame update
    void Start()
    {
        // if (loadingScreenShow)
        // {
        //     LoadngPanel.SetActive(true);
        // }

#if UNITY_EDITOR
        StartCoroutine(ShowHomecreenScreen());
#else
        if (!GameManager.Instance.InitModule)
            PlugInTest.InitAndroidModule();
        else
        {
            StartCoroutine(ShowHomecreenScreen());
        }
#endif
        // 
    }

    private IEnumerator ShowHomecreenScreen()
    {
        yield return new WaitForSeconds(1);
        ShowHomeScreen();
    }

    public void ShowHomeScreen()
    {
        if (!GameManager.Instance.InitModule)
            GameManager.Instance.InitModule = true;

#if UNITY_EDITOR
        GameManager.Instance.GameStart = true;
        PlugInTest.OpenMainScreen();
#else
        PlugInTest.WatchInterAds(b =>
        {
            GameManager.Instance.GameStart = true;
            PlugInTest.OpenMainScreen();
        });
        //
        // GameManager.Instance.GameStart = true;
        // PlugInTest.OpenMainScreen();
#endif
        
    }
}