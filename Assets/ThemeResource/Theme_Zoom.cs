using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

// [ExecuteInEditMode]
[AddComponentMenu("Theme/ThemeZoom")]
public class Theme_Zoom : MonoBehaviour {

	public RawImage rawImage;
    // public Material mat;
    public string variable;
    public AnimationCurve anm;
    public float Mul=1;
    public float Speed=1;
	// Use this for initialization
	public void Start()
	{
		// rawImage.texture = GameManager.Instance.SelectedImages[0];
		rawImage.material.SetFloat("_GrayScale_Fade_1",0f);
		Applytheme();
	}
	
	public void Applytheme()
	{
		rawImage.transform.localScale = Vector3.one;
		rawImage.texture = GameManager.Instance.SelectedImages[0];
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if ((SceneManager.GetActiveScene().buildIndex == 1)? AudioManager.Instance.isPlaying : RecordManager.Instance.Audio.isPlaying) 
	    {
		    if (rawImage.material!=null) rawImage.material.SetFloat(variable, anm.Evaluate(Time.time * Speed)*Mul);
	    }
    }
}
