﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Parent class responsible for extracting beats from..
/// ..spectrum value given by AudioSpectrum.cs
/// </summary>
public class AudioSyncer : MonoBehaviour {

	
	public enum Biass
	{
		A5 = 5,
		A6 = 6,
		A8 = 8,
		A10 = 10,
		A11 = 11,
		A12 = 12,
		A13 = 13,
		A14 = 14,
		A15 = 15,
		A17 = 17,
		A18 = 18,
		A19 = 19,
		A20 = 20,
		A21 = 21,
		A22 = 22,
		A23 = 23,
		A24 = 24,
		A25 = 25,
		A26 = 26,
		A27 = 27,
		A28 = 28,
		A29 = 29,
		A30 = 30,
		A31 = 31,
		A32 = 32,
		A35 = 35,
		A37 = 37,
		A38 = 38,
		A40 = 40,
		A42 = 42,
		A45 = 45,
		A48 = 48,
		A50 = 50
	};

	/// <summary>
	/// Inherit this to cause some behavior on each beat
	/// </summary>
	public virtual void OnBeat()
	{
		// Debug.Log("beat");
		m_timer = 0;
		m_isBeat = true;
		//print("OnBeat : AudioSyncer"  );
	}

	/// <summary>
	/// Inherit this to do whatever you want in Unity's update function
	/// Typically, this is used to arrive at some rest state..
	/// ..defined by the child class
	/// </summary>
	public virtual void OnUpdate()
	{
		
		// update audio value
		m_previousAudioValue = m_audioValue;
		m_audioValue = AudioSpectrum.spectrumValue;

		// if audio value went below the bias during this frame
		if (m_previousAudioValue > (float) bias &&
			m_audioValue <= (float) bias)
		{
			// if minimum beat interval is reached
			//print("OnBeat : AudioSyncer");
			if (m_timer > timeStep)
				OnBeat();
		}

		// if audio value went above the bias during this frame
		if (m_previousAudioValue <= (float) bias &&
			m_audioValue > (float) bias)
		{
			// if minimum beat interval is reached
			if (m_timer > timeStep)
				OnBeat();
		}

		m_timer += Time.deltaTime;
	}

	private void Update()
	{
		OnUpdate();
	}

	public Biass bias =  Biass.A5;
	public float timeStep = 0.02f;
	public float timeToBeat = 0.05f;
	public float restSmoothTime = 5f;

	private float m_previousAudioValue;
	private float m_audioValue;
	private float m_timer;

	protected bool m_isBeat;
}
