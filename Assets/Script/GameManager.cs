﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using MyPlugIn;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public ParticalData particleDeta = null;
    public Texture2D SelectedImage;
    public int SelectedThemeIndex = 0;
    public int SelectedTransitionIndex = 0;
    public AudioClip SelectedAudio;
    public GameObject SelectedParticalPrefab;
    public Texture2D[] SelectedImages;

    public int VideoWidth = 720;
    public string SaveVideoPath;
    public bool GameStart = false;
    public bool Watermark = true;
    public int OverlayIndex = 0;
    public bool isTamplate;
    public String tamplateName;
    public String tamplateUserName;

    
    public bool InitModule = false;
    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;

        DontDestroyOnLoad(gameObject);
    }


    private void Start()
    {
#if UNITY_EDITOR
#else
        Screen.fullScreen = false;
        Utils.setStatusBarValue(2048); // WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN
#endif
    }

    // IEnumerator Start()
    // {
    //     yield return new WaitForSeconds(2);
    //     OpenMainScreen();
    // }

    public void OpenMainScreen()
    {
#if UNITY_ANDROID
        // string filePath = Utils.JsonPath + Utils.JsonFileName;
        // Debug.LogError("Json load Path :"+filePath);
        // if (File.Exists(filePath))
        // {
        //     string dataAsJson = File.ReadAllText(filePath);
        //     particleDeta = JsonUtility.FromJson<ParticalData>(dataAsJson);
        //     Debug.LogError("loaded json data : "+particleDeta.toString());
        //     Debug.LogError("loaded json partical size : "+particleDeta.getParticalDetails().Count);
        // }
        // else
        // {
        //     Debug.LogError("Cannot load json data");
        // } 

        // PlugInTest.OpenMainScreen();
#endif
    }

    public IEnumerator OpenPreviewScreen(String openFrom)
    {
        Debug.LogError("OpenPreviewScreen 1 : " + openFrom);
        yield return new WaitForSeconds(1f);

        // if (!(SceneManager.GetActiveScene().buildIndex == 1))
            SceneManager.LoadScene(1);
        // else
        // {
        //     PreviewScreenManager.Instance.SetUpAllRelatedData();
        // }
    }

    // public string url = Application.dataPath + "/Resources/audio1.wav"; 
    // Application.persistentDataPath;
    //"C:/simFIL/Audio/audio2015-10-23 14h59m04s.wav";
    // public AudioSource source;


    IEnumerator LoadTexture(string url, int i)
    {
        WWW image = new WWW(url);
        yield return image;
        Texture2D texture = new Texture2D(1, 1);
        image.LoadImageIntoTexture(texture);
        SelectedImages[i] = texture;
        Debug.LogError("Load Image : " + url);
    }

    public void ImagesUpdate(string[] paths, String musicName, String particleName, bool isTemplatee,String tamplateName, String tamplateUserName)
    {
        Array.Clear(SelectedImages, 0, SelectedImages.Length);

        isTamplate = isTemplatee;
        Debug.LogError("IsTemplate : "+isTemplatee);
        this.tamplateName = tamplateName;
        this.tamplateUserName = tamplateUserName;

        SelectedImages = new Texture2D[paths.Length];
        for (int i = 0; i < paths.Length; i++)
        {
            StartCoroutine(LoadTexture("file:///" + paths[i], i));
        }

        if (isTemplatee)
        {
            StartCoroutine(LoadAudio(musicName));
            StartCoroutine(LoadBundle1(particleName));
        }

        // else
        // {
        StartCoroutine(OpenPreviewScreen("GameManager ImageUpdates"));
        // }

        // if (SceneManager.GetActiveScene().buildIndex == 1)
        //     PreviewScreenManager.Instance.OnImageChange();
        // StartCoroutine(OpenPreviewScreen());
    }

    public IEnumerator LoadAudio(string path)
    {
        Debug.LogError("LoadClip Start : " + path);
        string FullPath = "file://" + path;
        WWW URL = new WWW(FullPath);
        yield return URL;
        //GameManager.Instance.SelectedAudio = NAudioPlayer.FromMp3Data(URL.bytes);
        SelectedAudio = URL.GetAudioClip();
        Debug.LogError("Load Audio Success : " + SelectedAudio.name);
    }

    public IEnumerator LoadBundle1(String paths)
    {
        // var bundleLoadRequest = AssetBundle.LoadFromFileAsync(paths);
        // yield return bundleLoadRequest;
        Debug.LogError("@@@ LoadBundle122 : " + paths);
        if (File.Exists(paths))
        {
            Debug.LogError("@@@ LoadBundle122 file  exist : " + paths);
        }

        // else
        // {
        byte[] stream; // = File.ReadAllBytes(paths);

        stream = File.ReadAllBytes(paths);
        Debug.LogError("@@@ Stream122 Byte size! " + stream.Length);
        //decrypt
        Utils.Decrypt(ref stream);

        // MemoryStream memory = new MemoryStream(stream);
        AssetBundle myLoadedAssetBundle = AssetBundle.LoadFromMemory(stream);

        // var myLoadedAssetBundle = bundleLoadRequest.assetBundle;
        // Debug.LogError("@@@ one1 : "+SubCatData.getPrefbName());
        if (myLoadedAssetBundle == null)
        {
            Debug.LogError("@@@ Failed122 to load AssetBundle!");
            yield break;
        }

        Debug.LogError("@@@ loaded122 AssetBundle!  :: " + Path.GetFileNameWithoutExtension(paths));
        var assetLoadRequest = myLoadedAssetBundle.LoadAssetAsync<GameObject>(Path.GetFileNameWithoutExtension(paths));
        yield return assetLoadRequest;

        SelectedParticalPrefab = assetLoadRequest.asset as GameObject;

        myLoadedAssetBundle.Unload(false);
        // }

        Debug.LogError("Loaded PrefabName : " + SelectedParticalPrefab.name);
        // StartCoroutine(OpenPreviewScreen("GameManager LoadBundle1"));
    }

    public IEnumerator LoadBundle(String paths)
    {
        // Debug.LogError("partical file name :"+Path.GetFileName(paths));
        // Debug.LogError("partical path :"+paths);
        // var bundleLoadRequest = AssetBundle.LoadFromFileAsync(Path.Combine(Utils.ExternalParticalPrefabPath + Path.GetFileName(paths)));
        // yield return bundleLoadRequest;

        Debug.LogError("LoadBundle : " + paths);

        byte[] stream; // = File.ReadAllBytes(paths);

        WWW dbPath = new WWW(paths);
        while (!dbPath.isDone)
        {
        }

        if (!string.IsNullOrEmpty(dbPath.error))
        {
            //handle www error?
            Debug.LogError("error : " + dbPath.error);
        }
        else
        {
            stream = dbPath.bytes;
            //decrypt
            Utils.Encrypt(ref stream);

            AssetBundle myLoadedAssetBundle = AssetBundle.LoadFromMemory(stream);

            if (myLoadedAssetBundle == null)
            {
                Debug.Log("Failed to load AssetBundle!");
                yield break;
            }

            var assetLoadRequest = myLoadedAssetBundle.LoadAssetAsync<GameObject>(Path.GetFileName(paths));
            yield return assetLoadRequest;


            SelectedParticalPrefab = assetLoadRequest.asset as GameObject;

            myLoadedAssetBundle.Unload(false);
        }

        Debug.LogError("Loaded PrefabName : " + SelectedParticalPrefab.name);
        StartCoroutine(OpenPreviewScreen("1"));
    }

    void OnApplicationPause(bool pauseStatus)
    {
        if (!pauseStatus)
        {
            //Show IN APP ADS HERE 
        }
    }


    // public IEnumerator LoadClip(string path)
    // {
    //     Debug.LogError("LoadClip : "+path);
    //     AudioClip clip = null;
    //     WWW image = new WWW (path);
    //     yield return image;
    //     SelectedAudio = image.GetAudioClip();
    //     Debug.LogError("Load Audio Success : "+SelectedAudio.name);
    //     AudioManager.Instance.ResetAudio();
    // }

    // IEnumerator LoadAudio(string  path)
    // {
    //     Debug.LogError("LoadClip Start : "+path);
    //     string FullPath = "file:///" + path;
    //     WWW URL = new WWW(FullPath);
    //     yield return URL;
    //
    //     SelectedAudio = URL.GetAudioClip(false, true);
    //     Debug.LogError("Load Audio Success : "+SelectedAudio.name);
    //     
    // }
    //
    // IEnumerator LoadSong(string path)
    // {
    //     string url = string.Format("file://{0}", path); 
    //     WWW www = new WWW(url);
    //     yield return www;
    //     SelectedAudio = www.GetAudioClip(false, false);
    //     Debug.LogError("Load Audio Success : "+SelectedAudio.name);
    //     AudioManager.Instance.ResetAudio();
    // }


    // // async Task<AudioClip> LoadClip(string path)
    // {
    //     AudioClip clip = null;
    //     using (UnityWebRequest uwr = UnityWebRequestMultimedia.GetAudioClip(path, AudioType.MPEG))
    //     {
    //         uwr.SendWebRequest();
    //
    //         // wrap tasks in try/catch, otherwise it'll fail silently
    //         try
    //         {
    //             while (!uwr.isDone) await Task.Delay(5);
    //
    //             if (uwr.isNetworkError || uwr.isHttpError) Debug.Log($"{uwr.error}");
    //             else
    //             {
    //                 clip = DownloadHandlerAudioClip.GetContent(uwr);
    //             }
    //         }
    //         catch (Exception err)
    //         {
    //             Debug.Log($"{err.Message}, {err.StackTrace}");
    //         }
    //     }
    //
    //     return clip;
    // }
}