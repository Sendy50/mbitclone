

using System;
using System.Collections.Generic;

[Serializable]
public class ParticalDetailsItem{
	
	public String ParticleCatName;

	public int ParticalCatOnline;
	
	public int ParticleCatId;

	public List<ParticalInfoItem> ParticalInfo;
	
	public String ParticalCatImg;

	public void setParticleCatName(String particleCatName){
		this.ParticleCatName = particleCatName;
	}

	public String getParticleCatName(){
		return ParticleCatName;
	}

	public void setParticalCatOnline(int particalCatOnline){
		this.ParticalCatOnline = particalCatOnline;
	}

	public int getParticalCatOnline(){
		return ParticalCatOnline;
	}

	public void setParticleCatId(int particleCatId){
		this.ParticleCatId = particleCatId;
	}

	public int getParticleCatId(){
		return ParticleCatId;
	}

	public void setParticalInfo(List<ParticalInfoItem> particalInfo){
		this.ParticalInfo = particalInfo;
	}

	public List<ParticalInfoItem> getParticalInfo(){
		return ParticalInfo;
	}

	public void setParticalCatImg(String particalCatImg){
		this.ParticalCatImg = particalCatImg;
	}

	public String getParticalCatImg(){
		return ParticalCatImg;
	}
	
 	public String toString(){
		return 
			"ParticalDetailsItem{" + 
			"particleCatName = '" + ParticleCatName + '\'' + 
			",particalCatOnline = '" + ParticalCatOnline + '\'' + 
			",particleCatId = '" + ParticleCatId + '\'' + 
			",particalInfo = '" + ParticalInfo + '\'' + 
			",particalCatImg = '" + ParticalCatImg + '\'' + 
			"}";
		}
}