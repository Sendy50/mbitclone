﻿using System;
using System.Collections;
using System.Collections.Generic;
using GoogleMobileAds.Api;
using UnityEngine;
using UnityEngine.UI;


public class AdmobNativePanel : MonoBehaviour
{
    public static AdmobNativePanel Instance; 
    //public bool isViewNative = false;
    private bool unifiedNativeAdLoaded;
    private UnifiedNativeAd nativeAd;


    public const string TemplateId = "10085730";

    public GameObject NativePanell;
    public RawImage AdsIcon;
    public RawImage Banner;
    public bool  ShowBanner = true;
    public RawImage AdsChoiceIcon;
    public Text Adstext;
    // public Text Adsverttext;
    public Text Btntext;
    public Text Bodytext;
    // public GameObject ABNativePanell;

    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        // MobileAds.Initialize(initStatus => { });
        if (!AdsManager.Instance.AdsOn)
        {
            return;
        }
        
        RequestNativeAd();
    }

    private void RequestNativeAd()
    {
        Debug.LogError("@Unified RequestNativeAd : id : "+AdsManager.Instance.nativeAdUnitId);
        AdLoader adLoader = new AdLoader.Builder(AdsManager.Instance.nativeAdUnitId)
            .ForUnifiedNativeAd()
            .Build();
        adLoader.OnUnifiedNativeAdLoaded += this.HandleUnifiedNativeAdLoaded;
        adLoader.OnAdFailedToLoad += this.HandleNativeAdFailedToLoad;
        adLoader.LoadAd(new AdRequest.Builder().Build());
    }

    private void HandleUnifiedNativeAdLoaded(object sender, UnifiedNativeAdEventArgs args)
    {
        Debug.LogError("@Unified native ad loaded.");
        this.nativeAd = args.nativeAd;
        this.unifiedNativeAdLoaded = true;
    }
    
    private void HandleNativeAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        Debug.LogError("@Unified Native ad failed to load: " + args.Message);
    }
    
    // Start is called before the first frame update
    public void Update()
    {
        if (this.unifiedNativeAdLoaded)
        {
            this.unifiedNativeAdLoaded = false;
            NativePanell.SetActive(true);
            // NativePanell.transform.GetChild(0).gameObject.SetActive(true);
            // NativePanell.transform.GetChild(1).gameObject.SetActive(false);
            AdsIcon.texture = nativeAd.GetIconTexture();
            
            if (ShowBanner)
            {
                Debug.LogError("Banner.rectTransform.sizeDelta 1 : "+ Banner.rectTransform.sizeDelta);
                Texture2D texture = nativeAd.GetImageTextures()[0];
                Debug.LogError("width : "+ texture.width + "height : "+ texture.height);
                Banner.rectTransform.sizeDelta = new Vector2( Banner.rectTransform.sizeDelta.x,(int)(texture.width * Banner.texture.height) / texture.height);
                Banner.texture = texture;
                Debug.LogError("Banner.rectTransform.sizeDelta 2 : "+ Banner.rectTransform.sizeDelta);
            }

            AdsChoiceIcon.texture = nativeAd.GetAdChoicesLogoTexture();
            Adstext.text = nativeAd.GetHeadlineText();
            // Adsverttext.text = this.nativeAd.GetAdvertiserText();
            Btntext.text = nativeAd.GetCallToActionText();
            Bodytext.text = nativeAd.GetBodyText();

            if (ShowBanner)
            {
                List<GameObject> ImageList = new List<GameObject>();
                ImageList.Add(Banner.gameObject);
                nativeAd.RegisterImageGameObjects(ImageList);
            }

            nativeAd.RegisterIconImageGameObject(AdsIcon.gameObject);
            nativeAd.RegisterAdChoicesLogoGameObject(AdsChoiceIcon.gameObject);
            nativeAd.RegisterHeadlineTextGameObject(Adstext.gameObject);    
            // nativeAd.RegisterAdvertiserTextGameObject(Adsverttext.gameObject);
            nativeAd.RegisterCallToActionGameObject(Btntext.gameObject);
            nativeAd.RegisterBodyTextGameObject(Bodytext.gameObject);
            
        }
    }
}
