﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[AddComponentMenu("Theme/BlinkerZoom")]
public class Theme_BlinkerZoom : AudioSyncer
{
    public bool prefab;
    public RawImage rawImage;
    // public Material ThemeMat;

    public void Start()
    {
        // rawImage.texture = GameManager.Instance.SelectedImages[0];
        rawImage.material.SetFloat("_GrayScale_Fade_1",0f);
        Applytheme();
    }

    public void Applytheme()
    {
        rawImage.transform.localScale = Vector3.one;
        rawImage.texture = GameManager.Instance.SelectedImages[0];
    }
    
    
   
    
    private IEnumerator DoDark()
    {
        // gameObject.GetComponent<Renderer>().sharedMaterial.SetFloat("_YourParameter", someValue);
        // Debug.LogError("DoDark");
        rawImage.material.SetFloat("_GrayScale_Fade_1",0f);
        rawImage.transform.localScale = new Vector3(1.05f,1.05f,1.05f);
        yield return new WaitForSeconds(0.1f);
        // OnBeatt = false;
        m_isBeat = false;
        // Debug.LogError("DoDark");
    }
    
    public override void OnUpdate()
    {
        base.OnUpdate();

        if (m_isBeat) return;
        rawImage.material.SetFloat("_GrayScale_Fade_1",1f);
        rawImage.transform.localScale = Vector3.one;
    }
    
    public override void OnBeat()
    {
        base.OnBeat();
        // Debug.LogError("OnBeat");
        StopCoroutine(DoDark());
        StartCoroutine(DoDark());
        // OnBeatt = true;
    }
}
