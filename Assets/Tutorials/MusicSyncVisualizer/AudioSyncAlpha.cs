using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioSyncAlpha : AudioSyncer {
	
	
    private IEnumerator MoveToScale(float _target)
    {
        float _curr = transform.GetComponent<Image>().color.a;
        float _initial = _curr;
        float _timer = 0;
		

        while (_curr != _target)
        {
            _curr = Mathf.Lerp(_initial, _target, _timer / timeToBeat);
            _timer += Time.deltaTime;
            
            transform.GetComponent<Image>().color = new Color(transform.GetComponent<Image>().color.r, transform.GetComponent<Image>().color.g, transform.GetComponent<Image>().color.b, _curr);

            yield return null;
        }

        m_isBeat = false;
    }

    public override void OnUpdate()
    {
        base.OnUpdate();

        if (m_isBeat) return;

        // transform.localScale = Vector3.Lerp(transform.localScale, restScale, restSmoothTime * Time.deltaTime);
        float newAlpha = Mathf.Lerp(transform.GetComponent<Image>().color.a, restAlpha, restSmoothTime * Time.deltaTime);
        transform.GetComponent<Image>().color = new Color(transform.GetComponent<Image>().color.r, transform.GetComponent<Image>().color.g, transform.GetComponent<Image>().color.b, newAlpha);
    }

    public override void OnBeat()
    {
        base.OnBeat();

        StopCoroutine("MoveToScale");
        StartCoroutine("MoveToScale", beatAlpha);
    }

	
	
    // [SerializeField] public Vector3 beatScale = new Vector3(1.2f,1.2f,1.2f);
    // [SerializeField] public Vector3 restScale = Vector3.one;

    [SerializeField] public float beatAlpha = 1f;
    [SerializeField] public float restAlpha = 0f;
    
}