﻿using System.Collections;
using UnityEditor;
using System.IO;
using UnityEngine;

public class CreateAssetBundles
{
    private static string folderName;
    private static string filePath;

    [MenuItem("Assets/Build AssetBundle")]
    static void ExportResource()
    {
        folderName = "AssetBundles";
        filePath = Path.Combine(Application.streamingAssetsPath, folderName);

        //Build for Windows platform
        // BuildPipeline.BuildAssetBundles(filePath, BuildAssetBundleOptions.None, BuildTarget.StandaloneWindows64);

        //Uncomment to build for other platforms
        //BuildPipeline.BuildAssetBundles(filePath, BuildAssetBundleOptions.None, BuildTarget.iOS);
        BuildPipeline.BuildAssetBundles(filePath, BuildAssetBundleOptions.None, BuildTarget.Android);
        //BuildPipeline.BuildAssetBundles(filePath, BuildAssetBundleOptions.None, BuildTarget.WebGL);
        //BuildPipeline.BuildAssetBundles(filePath, BuildAssetBundleOptions.None, BuildTarget.StandaloneOSX);

        //Refresh the Project folder
        AssetDatabase.Refresh();
        
    }
    
   
}