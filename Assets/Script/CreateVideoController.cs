using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text.RegularExpressions;
using MyPlugIn;
// using Unity.UNetWeaver;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;
using Random = UnityEngine.Random;

public class CreateVideoController : MonoBehaviour
{
    public GameObject ReportDialog;
    public GameObject ThanksDialog;
    public GameObject RadioButton1;
    public GameObject RadioButton2;
    public GameObject RadioButton3;
    public Text LogText;
    public Text viewText;
    public Text editText;
    public Text tamplateNameText;
    public Text tamplateUserNameText;
    public Text tellUsText;
    public Text emailText;
    private bool tellUs;
    private bool email;


    public const string MatchEmailPattern =
        @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
        + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
        + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
        + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";


    // Start is called before the first frame update
    void Start()
    {
        Debug.LogError("@@@@@ Time Now : " + DateTime.Now);
        viewText.text = GetViewCount(GameManager.Instance.tamplateName);
        editText.text = GetEditCount(GameManager.Instance.tamplateName);
        tamplateNameText.text = GameManager.Instance.tamplateName;
        tamplateUserNameText.text = GameManager.Instance.tamplateUserName;
    }

    private string GetViewCount(string tamplateName)
    {
        int pastview = PlayerPrefs.GetInt(tamplateName, Random.Range(10, 50));
        String vieww = "";
        pastview = pastview + Random.Range(50, 300);
        if (pastview > 999)
        {
            vieww = (int) (pastview / 1000) + "K";
        }
        else
        {
            vieww = "" + pastview;
        }

        PlayerPrefs.SetInt(tamplateName, pastview);
        PlayerPrefs.Save();
        return vieww;
    }

    private string GetEditCount(string tamplateName)
    {
        int pastview = PlayerPrefs.GetInt(tamplateName+"_Edit", Random.Range(5, 30));
        String vieww = "";
        pastview = pastview + Random.Range(30, 100);
        if (pastview > 999)
        {
            vieww = (int) (pastview / 1000) + "K";
        }
        else
        {
            vieww = "" + pastview;
        }

        PlayerPrefs.SetInt(tamplateName+"_Edit", pastview);
        PlayerPrefs.Save();
        return vieww;
    }

    public void OnBackBBtnClick()
    {
        PlugInTest.OpenMainScreen();
        SceneManager.LoadScene(0);
    }


    public void OnShareBBtnClick()
    {
        PlugInTest.OpenShare();
    }


    public void OnRadioButtonClick(int index)
    {
        switch (index)
        {
            case 1:
                RadioButton1.SetActive(true);
                RadioButton2.SetActive(false);
                RadioButton3.SetActive(false);
                break;
            case 2:
                RadioButton1.SetActive(false);
                RadioButton2.SetActive(true);
                RadioButton3.SetActive(false);
                break;
            case 3:
                RadioButton1.SetActive(false);
                RadioButton2.SetActive(false);
                RadioButton3.SetActive(true);
                break;
        }
    }


    public void OnTellUsTextSet(String value)
    {
        // tellUsText = value;
        Debug.LogError("@@@@@ OnTellUsTextSet : " + value);
        if (tellUsText.text.Length > 5)
            tellUs = true;
        else
            StartCoroutine(Loger("Please Enter Atleast 5 Words"));
    }


    public void OnEmailTextSet(String value)
    {
        // emailText = value;
        Debug.LogError("@@@@@ OnEmailTextSet : " + value);
        if (Regex.IsMatch(emailText.text, MatchEmailPattern))
            tellUs = true;
        else
            StartCoroutine(Loger("Please Enter Valid Email"));
    }


    private IEnumerator Loger(string log)
    {
        LogText.text = log;
        LogText.gameObject.SetActive(true);
        yield return new WaitForSeconds(3);
        LogText.gameObject.SetActive(false);
    }


    public void OnSubmitReportBtnClick()
    {
        if (tellUsText.text.Length <= 0)
        {
            StartCoroutine(Loger("Please enter atleast 5 words about report"));
            return;
        }
        else if (emailText.text.Length <= 0)
        {
            StartCoroutine(Loger("Please enter valid Email"));
            return;
        }

        ShowReportDialog(false);
        ThanksDialog.SetActive(true);
    }

    public void OkThanksBtnbClick()
    {
        ThanksDialog.SetActive(false);
    }

    public void ShowReportDialog(bool show)
    {
        if (show)
        {
            // if (PlayerPrefs.GetInt("Report", 0) == 1)
            // {
            //     _ShowAndroidToastMessage("You have Already reported");
            // }
            ReportDialog.SetActive(true);
        }
        else
            ReportDialog.SetActive(false);
    }


    // private void _ShowAndroidToastMessage(string message)
    // {
    //     AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
    //     AndroidJavaObject unityActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
    //
    //     if (unityActivity != null)
    //     {
    //         AndroidJavaClass toastClass = new AndroidJavaClass("android.widget.Toast");
    //         unityActivity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
    //         {
    //             AndroidJavaObject toastObject = toastClass.CallStatic<AndroidJavaObject>("makeText", unityActivity, message, 0);
    //             toastObject.Call("show");
    //         }));
    //     }
    // }
}