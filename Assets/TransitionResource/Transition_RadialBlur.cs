﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Transition_RadialBlur : MonoBehaviour
{
    public float BlendSpeed = 5.0f;
    private float fader = 0f;
    private float timer;
    private int direction = 1;
    int toPicture;
    int shader;
    int counter;

    public Texture[] aImages;
    private bool wait = false;
    private bool callWaitUpdate = true;
    private Material mat;
    private bool abc = true;

    // private void OnEnable()
    // {
    //     if (SceneManager.GetActiveScene().buildIndex == 1)
    //         AudioManager.Instance.OnPlayerPlayPause += MusicPlaying;
    // }
    //
    //
    // private void MusicPlaying(bool play)
    // {
    //     // Debug.LogError("music playing on fade: " + play);
    //     if (play)
    //     {
    //         Applytheme();
    //     }
    // }
    //
    //
    // private void OnDisable()
    // {
    //     if (SceneManager.GetActiveScene().buildIndex == 1)
    //         AudioManager.Instance.OnPlayerPlayPause -= MusicPlaying;
    // }

    
    public void Applytheme()
    {
        Debug.LogError("Transition_Fade Start : "+gameObject.name);
        fader = 0;
        wait = false;
        toPicture = 0;
        mat = GetComponent<MeshRenderer>().sharedMaterial;
        aImages = new Texture2D[GameManager.Instance.SelectedImages.Length];
        System.Array.Copy(GameManager.Instance.SelectedImages, aImages, GameManager.Instance.SelectedImages.Length);
        mat.SetFloat("_Blend", 0.2f);
        mat.SetTexture("_MainTex", (Texture) aImages[toPicture]);
        toPicture = (toPicture + 1) >= aImages.Length ? 0 : (toPicture + 1);
        mat.SetTexture("_MainTex2", (Texture) aImages[toPicture]);
        StartCoroutine(Wait());
    }

    private void Start()
    {
       Applytheme();
    }

   

    private void Update()
    {
        if ((SceneManager.GetActiveScene().buildIndex == 1)
            ? !AudioManager.Instance.isPlaying
            : !RecordManager.Instance.Audio.isPlaying)
        {
            if (counter != 0)
            {
                Applytheme();
            }
            return;
        }
        
        if(!wait)
            UpdateBlend();
    }
    
    public void UpdateBlend()
    {
        fader += Time.deltaTime * BlendSpeed * direction;
        fader = Mathf.Clamp(fader, 0, 1);
       
        if (fader == 0 || fader == 1)
        {
            StartCoroutine(Wait());
        }

        if (fader == 0)
        {
            direction *= -1;
            toPicture = (toPicture + 1) >= aImages.Length ? 0 : (toPicture + 1);
            mat.SetTexture("_MainTex2", (Texture) aImages[toPicture]);
        }
        else if (fader == 1)
        {
            direction *= -1;
            toPicture = (toPicture + 1) >= aImages.Length ? 0 : (toPicture + 1);
            mat.SetTexture("_MainTex", (Texture) aImages[toPicture]);
        }
        else
        {
            mat.SetFloat("_Blend", fader);
        }
    }
    
    public IEnumerator Wait()
    {
        mat.SetFloat("_Blend", fader);
        wait = true;
        yield return new WaitForSeconds(3);
        wait = false;
    }
}