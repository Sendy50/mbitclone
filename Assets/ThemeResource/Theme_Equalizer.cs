﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Theme_Equalizer : MonoBehaviour
{
    
    public Image rawImage;
    public GameObject AnimPanel;
    
    void Start()
    {
        Applytheme();
    }
    
    public void Applytheme()
    {
        rawImage.transform.localScale = Vector3.one;

        Sprite sprite = Sprite.Create(GameManager.Instance.SelectedImages[0],new Rect(0.0f, 0.0f,GameManager.Instance.SelectedImages[0].width,GameManager.Instance.SelectedImages[0].height),new Vector2(0.5f,0.5f),100.0f );
        rawImage.sprite = sprite;

        for(int i = 0; i < AnimPanel.transform.childCount; i++)
        {
            AnimPanel.transform.GetChild(i).transform.GetChild(0).GetComponent<Image>().sprite = sprite;
        }
        
    }
    
    
}
