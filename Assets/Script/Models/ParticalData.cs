

using System;
using System.Collections.Generic;

[Serializable]
public class ParticalData{


	public List<ParticalDetailsItem> ParticalDetails;

	public void setParticalDetails(List<ParticalDetailsItem> ParticalDetails){
		this.ParticalDetails = ParticalDetails;
	}

	public List<ParticalDetailsItem> getParticalDetails(){
		return ParticalDetails;
	}

 	public String toString(){
		return 
			"ParticalData{" + 
			"ParticalDetails = '" + ParticalDetails + '\'' + 
			"}";
		}
}