﻿using System;
using NatCorder;
using NatCorder.Clocks;
using NatCorder.Inputs;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using MyPlugIn;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RecordManager : MonoBehaviour
{
    public AudioSource Audio;

    private float CurrentAudioLength;

    // public RawImage Image;
    public GameObject ParticalPlace;
    public Image VideoProgress;
    public Text loadingtext;

    [Header("Recording")] public int videoWidth = 720;
    public int videoHeight = 1280;
    public bool recordMicrophone = true;

    public GameObject WatermarkPnael;
    public Image OverlayImage;
    public Sprite[] OverlaySprites;

    private IMediaRecorder videoRecorder;
    private CameraInput cameraInput;
    private AudioInput audioInput;
    private AudioSource microphoneSource;
    public static RecordManager Instance;
    private static Action<bool> InterAdss;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        VideoProgress.fillAmount = 0;
        Audio = GetComponent<AudioSource>();
        // CalculateVideoHeightWidth();    
        SetUpAllRelatedData();
        PlugInTest.sendEvent("User At Record Screen");
    }

    private void CalculateVideoHeightWidth()
    {
        // if (GameManager.Instance.VideoWidth == 720)
        // {
        Debug.LogError("Video Meta : W : " + Screen.width + " H : " + (1280 * Screen.width) / 720);
        videoWidth = Screen.width;
        videoHeight = (1280 * Screen.width) / 720;
        // }
        Debug.LogError("Video Meta : W : " + videoWidth + " H : " + videoHeight);
    }

    private void SetUpAllRelatedData()
    {
        // Image.texture = GameManager.Instance.SelectedImages[0];
        OverlayImage.gameObject.SetActive(false);
        if (GameManager.Instance.OverlayIndex != 0)
        {
            OverlayImage.gameObject.SetActive(true);
            OverlayImage.sprite = OverlaySprites[GameManager.Instance.OverlayIndex];
        }

        if (GameManager.Instance.Watermark)
            WatermarkPnael.SetActive(true);
        else
            WatermarkPnael.SetActive(false);

        Audio.clip = GameManager.Instance.SelectedAudio;
        GameObject ab = Instantiate(GameManager.Instance.SelectedParticalPrefab, ParticalPlace.transform);
        StartVideo();
    }

    public void StartVideo()
    {
        Audio.Play();
        StartRecording();
        StartCoroutine(IsPlaying());
    }

    private IEnumerator IsPlaying()
    {
        yield return new WaitUntil(() => !Audio.isPlaying);
        StopRecording();
        Audio.Stop();
    }

    private void Update()
    {
        if (Audio.isPlaying)
        {
            VideoProgress.fillAmount = Audio.time / Audio.clip.length;
            loadingtext.text = (int) (VideoProgress.fillAmount * 100) + "%";
        }
    }


    // private void Update()
    // {
    //     if (Audio.GetComponent<AudioSource>().isPlaying && CurrentAudioLength < Audio.GetComponent<AudioSource>().clip.length)
    //     {
    //         CurrentAudioLength = Audio.GetComponent<AudioSource>().time;
    //     }
    //     else
    //     {
    //         Audio.GetComponent<AudioSource>().Stop();
    //         SaveVid();
    //     }
    // }


    public void StartRecording()
    {
        // Start recording
        var frameRate = 30;
        var sampleRate = recordMicrophone ? AudioSettings.outputSampleRate : 0;
        var channelCount = recordMicrophone ? (int) AudioSettings.speakerMode : 0;
        var recordingClock = new RealtimeClock();
        videoRecorder = new MP4Recorder(
            videoWidth,
            videoHeight,
            frameRate,
            sampleRate,
            channelCount,
            recordingPath =>
            {
                Debug.LogError($"Saved recording to: {recordingPath}");
                // var prefix = Application.platform == RuntimePlatform.IPhonePlayer ? "file://" : "";
                // Handheld.PlayFullScreenMovie($"{prefix}{recordingPath}");

                string filename = Path.GetFileName(recordingPath);
                GameManager.Instance.SaveVideoPath = recordingPath;
                NativeGallery.SaveVideoToGallery(recordingPath, "MBitClone", filename,
                    (path) => Debug.LogError("Media save result: " + path + " "));
                //DeleteTempFile(recordingPath, filename);
                Debug.LogError("AppManager /SaveVideoPath : " + GameManager.Instance.SaveVideoPath);
                // AppManager.Instance.Previewpanel.GetComponent<PreviewVideoManager>().Start();

                Debug.LogError("ShowShareScreen");

               
                     // InterAdss += (b) =>
                     // {
                        // if(b)
                        // if (SceneManager.GetActiveScene().buildIndex == 2)
                        // {
                            Debug.LogError("SceneManager.GetActiveScene().buildIndex/... : " + SceneManager.GetActiveScene().buildIndex);
                            // Debug.LogError("InterAds Complete/... : " + b);
                            PlugInTest.OpenShareScreen();
                        // }
                    // };
                

                // PlugInTest.WatchInterAds(InterAdss);
            }
        );
        // Create recording inputs
        cameraInput = new CameraInput(videoRecorder, recordingClock, Camera.main);
        audioInput = recordMicrophone
            ? new AudioInput(videoRecorder, recordingClock, Audio.GetComponent<AudioSource>(), false)
            : null;
        // Unmute microphone
        Audio.GetComponent<AudioSource>().mute = audioInput == null;
    }

    private void DeleteTempFile(string filePath, string filename)
    {
        if (!File.Exists(filePath))
        {
            Debug.LogError(filename +
                           " file exists, deleting..."); //Debug.Log( fileName + " file exists, deleting..." );

            File.Delete(filePath);
        }
    }

    public void StopRecording()
    {
        // Stop recording
        audioInput?.Dispose();
        cameraInput?.Dispose();
        videoRecorder?.Dispose();
        Debug.LogError("Complete Recording...");
    }

    public void ShowPreviewPanel()
    {
        Debug.LogError("ShowPreviewPanel");
        SceneManager.LoadScene(1);
    }

    public void ShowHomePanel()
    {
        Debug.LogError("ShowHomePanel");
        SceneManager.LoadScene(0);
    }
}