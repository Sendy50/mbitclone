﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using MyPlugIn;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class SubCategoryItemController : MonoBehaviour
{
    public Image SubCatIcon;
    public Image SelectedImage;
    public Text SubCatName;
    public GameObject DownloadPanel;
    public Image DownloadProgress;
    public Image DownloadIcon;
    public GameObject PremiumPanel;
    // public GameObject Premiumicon;
    public GameObject ParticalPrefab;
    private ParticalInfoItem SubCatData;
    private Action<String> listener;
    private Action<bool> WatchAds;
    private AssetBundle bundle;
    private bool Downloaded = false;
    private UnityWebRequest uwr;
    private string bundleFolderName;
    private string bundleName;
    private string resourceName;
    private bool Downloading = false;
    public String PrefabPath;

    void Start()
    {
       
        GetComponent<Button>().onClick.AddListener(OnSubItemClick);
        listener += SetPrefab;
        WatchAds += Download;
    }

    private void OnDisable()
    {
        listener -= SetPrefab;
        WatchAds -= Download;
    }

    private void Download(bool iscomplete)
    {
        if (iscomplete)
        {
            PlugInTest.downloadParticle(SubCatData.getThemeName(),SubCatData.getAssestLink(), listener);
            Downloading = true;
            DownloadProgress.gameObject.SetActive(true);
            DownloadIcon.gameObject.SetActive(false);
        }
        else
        {
            Debug.LogError("Reward Failed from SubCategoryItemController");
        }
        
    }

    private void SetPrefab(String path)
    {
        // Debug.LogError("SetPrefab path: " + path);
        if (path.Equals(""))
        {
            Downloading = false;
            DownloadProgress.gameObject.SetActive(false);
            DownloadIcon.gameObject.SetActive(true);
            // DownloadPanel.SetActive(false);
            return;
        }
        Downloading = false;
        DownloadProgress.gameObject.SetActive(false);
        DownloadPanel.SetActive(false);
        StartCoroutine(LoadBundle1(path,true));//LoadBundle1
    }

    private void OnSubItemClick()
    {

        if (Downloading)
            return;
        
        if (SubCatData.getParticalOnline() == 1)
        {
            Debug.LogError("@@ Open Bundle Path + "+Path.Combine(Utils.ExternalParticalPrefabPath , SubCatData.getThemeName()));
            if (File.Exists(Path.Combine(Utils.ExternalParticalPrefabPath + SubCatData.getThemeName())))
            {
                Debug.LogError("OnSubItemClick Exist : "+ParticalPrefab.name);
                SubCategoryController.Instance.OnSubCategoryItemClick(ParticalPrefab);
                SelectedImage.gameObject.SetActive(true);
            }
            else if(SubCatData.getisPremium() == 1)
            {
                PreviewScreenManager.Instance.ShowWatchRewardDialog(WatchAds);
            }
            else
            {
                Debug.LogError("OnSubItemClick Not Exist : ");
                PlugInTest.downloadParticle(SubCatData.getThemeName(),SubCatData.getAssestLink(), listener);
                Downloading = true;
                DownloadProgress.gameObject.SetActive(true);
                DownloadIcon.gameObject.SetActive(false);
            }
        }
        else
        {
            // Debug.LogError("OnClick Prefab name : "+ParticalPrefab.name);
            SubCategoryController.Instance.OnSubCategoryItemClick(ParticalPrefab);
            SelectedImage.gameObject.SetActive(true);
        }
    }

    

    public void SetUp(ParticalInfoItem subCatData)
    {
        SubCatData = subCatData;
       
        GetSprite();

        Debug.LogError("SetUp Sub Cat : "+GameManager.Instance.SelectedParticalPrefab.name +" == "+SubCatData.getThemeName());
        
        if (SubCatData.getParticalOnline() == 1)
        {
            PrefabPath = Path.Combine(Utils.ExternalParticalPrefabPath + SubCatData.getThemeName());
            if (File.Exists(PrefabPath))
            {
                Debug.LogError("SetUp Exist : "+PrefabPath);
                StartCoroutine(LoadBundle1(PrefabPath,false));//LoadBundle1
                Downloaded = true;
                DownloadPanel.SetActive(false);
                PremiumPanel.SetActive(false);
                
                if (GameManager.Instance.SelectedParticalPrefab.name == SubCatData.getPrefbName())
                    SelectedImage.gameObject.SetActive(true);
            }
            else
            {
                Debug.LogError("SetUp Not Exist : "+PrefabPath);
                DownloadPanel.SetActive(true);
                if (SubCatData.getisPremium() == 1)
                    PremiumPanel.SetActive(true);
            }
        }
        else
        {
            PrefabPath = Path.Combine(Utils.InternalParticalPrefabPath + SubCatData.getThemeName());
            PremiumPanel.SetActive(false);
            DownloadPanel.SetActive(false);
            StartCoroutine(LoadBundle(PrefabPath,false));
            Downloaded = true;
            if (GameManager.Instance.SelectedParticalPrefab.name == SubCatData.getPrefbName())
                SelectedImage.gameObject.SetActive(true);
        }

        SubCatName.text = subCatData.getThemeName();
    }


    private void GetSprite()
    {
        string folderpath;
        Sprite sprite;
        // Debug.LogError("GetSprite : " + SubCatData.getParticalOnline());

        if (SubCatData.getParticalOnline() == 1)
        {
            folderpath =  SubCatData.getImgPath();
            StartCoroutine(GetSpriteFromPath(folderpath, SubCatIcon));
        }
        else
        {
            folderpath = Application.streamingAssetsPath + "/ParticalThumbImage/" + SubCatData.getImgPath();
            StartCoroutine(GetSpriteFromPath(folderpath, SubCatIcon));
        }
    }

    // private void GetPrefab()
    // {
    //     string folderpath;
    //     GameObject prefab;
    //     Debug.LogError("GetPrefab : " + SubCatData.getParticalOnline());
    //     Debug.LogError("ExternalParticalPrefabPath : " + Utils.ExternalParticalPrefabPath);
    //     Debug.LogError("InternalParticalPrefabPath : " + Utils.InternalParticalPrefabPath);
    //     
    //
    //     if (SubCatData.getParticalOnline() == 1)
    //     {
    //         folderpath = Utils.ExternalParticalPrefabPath + SubCatData.getPrefbName();
    //     }
    //     else
    //     {
    //         folderpath = Utils.InternalParticalPrefabPath + SubCatData.getThemeName();
    //     }
    //
    //     StartCoroutine(LoadAndInstantiateFromUnityWebRequest(false));
    //     // StartCoroutine(DownloadAssetBundle(folderpath, SubCatData.getPrefbName(), false));
    // }

    private IEnumerator GetSpriteFromPath(string path, Image catIcon)
    {
        
        Debug.LogError("@@ GetSpriteFromPath : "+path);
        
        using (uwr = UnityWebRequestTexture.GetTexture(path))
        {
            yield return uwr.SendWebRequest();
            if (uwr.isNetworkError || uwr.isHttpError)
            {
                Debug.LogError(uwr.error);
            }
            else
            {
                Texture2D tex = DownloadHandlerTexture.GetContent(uwr);
                Sprite fromTex = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height),
                    new Vector2(0.5f, 0.5f), 100.0f);
                catIcon.sprite = fromTex;
                catIcon.gameObject.transform.GetChild(0).gameObject.SetActive(false);
            }
        }
    }


    // private IEnumerator LoadAndInstantiateFromUnityWebRequest(String paths, bool b)
    // {
    //     // string uri = "";
    //     // if (SubCatData.getParticalOnline() == 1)
    //     // {
    //     //     uri = Application.persistentDataPath;
    //     // }
    //     // else
    //     // {
    //     //     uri = Application.streamingAssetsPath;
    //     // }
    //
    //     // (1) load asset bundle
    //
    //     // uri = Path.Combine(uri, "AssetBundles");
    //     // uri = Path.Combine(uri, SubCatData.ThemeName);
    //     
    //     Debug.LogError("Full Path : " + paths);
    //     UnityWebRequest request = UnityWebRequestAssetBundle.GetAssetBundle(paths, 0);
    //     yield return request.SendWebRequest();
    //     // (2) extract 'cube' from loaded asset bundle
    //     AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(request);
    //     ParticalPrefab = bundle.LoadAsset<GameObject>(SubCatData.getPrefbName());
    //
    //     Debug.LogError("Loaded PrefabName : "+ParticalPrefab.name);
    //     
    //     bundle.Unload(false);
    //     
    //     if (b)
    //          OnSubItemClick();
    // }
    
    public IEnumerator LoadBundle(String paths, bool b)
    {
        // var bundleLoadRequest = AssetBundle.LoadFromFileAsync(paths);
        // yield return bundleLoadRequest;
        Debug.LogError("LoadBundle : "+paths);
        if (File.Exists(paths))
        {
            Debug.LogError("LoadBundle file exist : "+paths);
        }

        byte[] stream;// = File.ReadAllBytes(paths);
        
        WWW dbPath = new WWW(paths);
        while (!dbPath.isDone) {}
        if (dbPath.error != null) {
            //handle www error?
            Debug.LogError("error : "+dbPath.error);
        }
        else
        {
            stream = dbPath.bytes;
            //decrypt
            Utils.Decrypt(ref stream);
            AssetBundle myLoadedAssetBundle = AssetBundle.LoadFromMemory(stream);

            // var myLoadedAssetBundle = bundleLoadRequest.assetBundle;
            if (myLoadedAssetBundle == null)
            {
                Debug.LogError("Failed to load AssetBundle!");
                yield break;
            }

            Debug.LogError("@@ one : "+SubCatData.getPrefbName());
            var assetLoadRequest = myLoadedAssetBundle.LoadAssetAsync<GameObject>(SubCatData.getPrefbName());
            yield return assetLoadRequest;

            ParticalPrefab = assetLoadRequest.asset as GameObject;
            Debug.LogError("@@ Second : "+ParticalPrefab.name);
            // GameManager.Instance.SelectedParticalPrefab = ParticalPrefab;

            myLoadedAssetBundle.Unload(false);
        }
        Debug.LogError("Loaded PrefabName : "+ParticalPrefab.name);
        
        if (b)
            OnSubItemClick();
    }

    IEnumerator DownloadAndCache(String paths, bool b)
    {
        WWW bundleRequest = new WWW(paths);
        while (!bundleRequest.isDone)
        {
            Debug.LogError("downloading....  "+bundleRequest.progress);
            yield return null;
        }
 
        AssetBundle bundle = null;
        if (bundleRequest.bytesDownloaded > 0)
        {
            AssetBundleCreateRequest myRequest = AssetBundle.LoadFromMemoryAsync(bundleRequest.bytes);
            while(!myRequest.isDone)
            {
                Debug.LogError("loading....");
                yield return null;
            }
            if(myRequest.assetBundle != null)
            {
                bundle = myRequest.assetBundle;
                GameObject model = null;
                if (bundle != null)
                {
                    
                    Debug.LogError("@@@ loaded AssetBundle!");
                    var assetLoadRequest = bundle.LoadAssetAsync<GameObject>(SubCatData.getPrefbName());
                    yield return assetLoadRequest;

                   
                    // Debug.LogError("@@@ Second1 : "+ParticalPrefab.name);
                    // GameManager.Instance.SelectedParticalPrefab = ParticalPrefab;
                    
                    
                    AssetBundleRequest newRequest = bundle.LoadAssetAsync<GameObject>(SubCatData.getPrefbName());
                    while (!newRequest.isDone)
                    {
                        Debug.LogError("loading ASSET....");
                        yield return null;
                    }
                    
                    ParticalPrefab = newRequest.asset as GameObject;
 
                    bundle.Unload(false);
                }
            }
            else
            {
                Debug.LogError("COULDN'T DOWNLOAD ASSET BUNDLE FROM URL: " + paths);
                // modelFound(null);
            }
        }
        else
        {
            Debug.LogError("COULDN'T DOWNLOAD ASSET BUNDLE FROM URL: " + paths);
            // modelFound(null);
        }
    }


    public IEnumerator LoadBundle1(String paths, bool b)
    {
        // var bundleLoadRequest = AssetBundle.LoadFromFileAsync(paths);
        // yield return bundleLoadRequest;
        Debug.LogError("@@@ LoadBundle1 : "+paths);
        if (File.Exists(PrefabPath))
        {
            Debug.LogError("@@@ LoadBundle1 file exist : "+PrefabPath);
        }

        byte[] stream;// = File.ReadAllBytes(paths);
        
        stream = File.ReadAllBytes(paths);
        Debug.LogError("@@@ Stream Byte size! "+stream.Length);
        //decrypt
        Utils.Decrypt(ref stream);

        // MemoryStream memory = new MemoryStream(stream);
        AssetBundle myLoadedAssetBundle = AssetBundle.LoadFromMemory(stream);

            // var myLoadedAssetBundle = bundleLoadRequest.assetBundle;
            Debug.LogError("@@@ one1 : "+SubCatData.getPrefbName());
            if (myLoadedAssetBundle == null)
            {
                Debug.LogError("@@@ Failed1 to load AssetBundle!");
                yield break;
            }

            Debug.LogError("@@@ loaded AssetBundle!");
            var assetLoadRequest = myLoadedAssetBundle.LoadAssetAsync<GameObject>(SubCatData.getPrefbName());
            yield return assetLoadRequest;

            ParticalPrefab = assetLoadRequest.asset as GameObject;
            // Debug.LogError("@@@ Second1 : "+ParticalPrefab.name);
            // GameManager.Instance.SelectedParticalPrefab = ParticalPrefab;

            myLoadedAssetBundle.Unload(false);

            if (b)
                OnSubItemClick();
    }

//     IEnumerator DownloadAssetBundle(string url, string bundleName, bool b)
//     {
// //Here Show Download Screen
//         yield return StartCoroutine(AssetBundleManager.downloadAssetBundle(url, 1));
//
// //Here Hide Download Screen
// // bundle = AssetBundle.LoadFromFile(url);
//         bundle = AssetBundleManager.getAssetBundle(url, 1);
//         if (bundle != null)
//             Debug.LogError("Download Success.... ");
//         else
//
//             Debug.LogError("Download error please retry");
//         GameObject obj = bundle.LoadAsset(bundleName) as GameObject;
//
// // bundle.Unload(bundleName);
//         SetPrefab(obj);
//         if (b)
//             OnSubItemClick();
//
//         bundle.Unload(false);
//         yield return null;
//     }

}