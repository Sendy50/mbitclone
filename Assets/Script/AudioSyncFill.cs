﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioSyncFill: AudioSyncer {

    private IEnumerator MoveToScale(float _target)
    {
        float _curr = GetComponent<Image>().fillAmount;
        float _initial = _curr;
        float _timer = 0;

        while (_curr != _target)
        {
            _curr = Mathf.Lerp(_initial, _target, _timer / timeToBeat);
            _timer += Time.deltaTime;
            GetComponent<Image>().fillAmount = _curr;

            yield return null;
        }

        m_isBeat = false;
    }

    public override void OnUpdate()
    {
        base.OnUpdate();

        // Debug.LogError("fillAmount : "+GetComponent<Image>().fillAmount);
        
        
        if (m_isBeat) return;

        // transform.localScale = Vector3.Lerp(transform.localScale, restScale, restSmoothTime * Time.deltaTime);

        GetComponent<Image>().fillAmount = Mathf.Lerp(beatFill,restFill,restSmoothTime * Time.deltaTime);
    }

    public override void OnBeat()
    {
        base.OnBeat();
        StopCoroutine("MoveToScale");
        StartCoroutine("MoveToScale", beatFill);
    }

    public float beatFill;
    public float restFill;
}
