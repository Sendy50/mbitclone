﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour
{
    
    public static AudioManager Instance;
    private AudioSource m_MusicSource;
    public Slider slider;
    public Text startText;
    public Text endText;
    private string timertext;

    public delegate void PlayerPlayPause(bool play);
    public event PlayerPlayPause OnPlayerPlayPause;
    private void Awake()
    {
        Instance = this;
    }

    private void Update()
    {
        if (isPlaying)
        {
            slider.value = m_MusicSource.time;
            timertext = ((int)m_MusicSource.time  <= 9) ? ("0" + (int)m_MusicSource.time) : ""+(int)m_MusicSource.time;
            startText.text = "00:" + timertext;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        m_MusicSource = GetComponent<AudioSource>();
        slider.minValue = 0;
        startText.text = "00:00";
        // PlayAudio(DefaultClip);
    }

    public void ResetAudio()
    {
        PlayAudio(GameManager.Instance.SelectedAudio);
    }
    
    public void MusicUpdate(string paths)
    {
        StartCoroutine( LoadAudio(paths));
    }
    
   
    public IEnumerator LoadAudio(string  path)
    {
        // string paths = "/storage/emulated/0/Download/iPhone 12 - Pinch.mp3";
        // string paths = "/storage/emulated/0/Android/data/com.aanipower.vidmash/files/music/1.mp3";
        Debug.LogError("LoadClip Start : "+path);
        string FullPath = "file://" + path;
        WWW URL = new WWW(FullPath);
        yield return URL;
        //GameManager.Instance.SelectedAudio = NAudioPlayer.FromMp3Data(URL.bytes);
        GameManager.Instance.SelectedAudio = URL.GetAudioClip();
        Debug.LogError("Load Audio Success : "+GameManager.Instance.SelectedAudio.name);
        PlayAudio(GameManager.Instance.SelectedAudio);
    }

   

    public bool isPlaying
    {
        get
        {
            return m_MusicSource.isPlaying;
        }
    }

    public void PlayAudio(AudioClip clip)
    {
        Debug.LogError("Play Audio : "+clip.name);
        GameManager.Instance.SelectedAudio = clip;
        m_MusicSource.clip = clip;
        m_MusicSource.Play();
        OnPlayerPlayPause(true);
        Transition.Instance.UpdateAllTrasition();
        startText.text = "00:00";
        endText.text = "00:"+(int)m_MusicSource.clip.length;
        slider.maxValue = m_MusicSource.clip.length;
        StartCoroutine(IsPlaying());
    }
    
    
    public void ResumeAudio()
    {
        m_MusicSource.Play();
        OnPlayerPlayPause(true);
        StartCoroutine(IsPlaying());
    }

    private IEnumerator IsPlaying()
    {
        yield return new WaitUntil(() => !isPlaying);
        StopAudio();
    }


    public void StopAudio()
    {
        Debug.LogError("Stop Audio : ");
        // if (!m_MusicSource.isPlaying)
        // {
        //     return;
        // }

        OnPlayerPlayPause(false);
        m_MusicSource.Stop();
        // }
    }
    
    
}
