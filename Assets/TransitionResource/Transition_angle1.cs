﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Transition_angle1 : MonoBehaviour {
	
	public float BlendSpeed = 5.0f;
	private float fader = 0f;
	private float timer;
	private int direction = 1;
	int toPicture;
	int shader;
	int counter;
	// Array of Pictures, assigned as Object
	public Texture[] aImages;
	public Texture[] aDissolver;
	private bool wait = false;
	private Material mat;


	public void Applytheme()
	{
		Debug.LogError("Transition_Angle1 Start : " + gameObject.name);
		counter = 0;
		fader = 0;
		wait = true;
		toPicture = 0;
		aImages = new Texture2D[GameManager.Instance.SelectedImages.Length];
		System.Array.Copy(GameManager.Instance.SelectedImages, aImages, GameManager.Instance.SelectedImages.Length);
		mat = GetComponent<MeshRenderer>().sharedMaterial;
		mat.SetFloat ("_Blend", 0f);
		mat.SetTexture("_MainTex", (Texture) aImages[toPicture]);
		toPicture = (toPicture + 1) >= aImages.Length ? 0 : (toPicture + 1);
		mat.SetTexture("_MainTex1", (Texture) aImages[toPicture]);
		mat.SetTexture ("_Gradient", (Texture)aDissolver[0]);
		StartCoroutine(Wait());
	}

	

	// Use this for initialization
	void Start () {	
		Applytheme();
	}
	
	
	private void Update()
	{
		if ((SceneManager.GetActiveScene().buildIndex == 1)
			? !AudioManager.Instance.isPlaying
			: !RecordManager.Instance.Audio.isPlaying)
		{
			if (counter != 0)
			{
				Applytheme();
			}
			return;
		}

		if(!wait)
			UpdateBlend();
	}
	
	
	// Update is called once per frame
	void UpdateBlend () {
		
		fader += Time.deltaTime * BlendSpeed * direction;
		fader = Mathf.Clamp (fader, 0, 1);

		if (fader == 0 || fader == 1)
		{
			StartCoroutine(Wait());
		}

		if (fader == 0) {
			counter = (counter + 1) % 1;
			if (counter == 0) {
				shader = (shader + 1) % aDissolver.Length;
			}
		
			direction *= -1;
			toPicture = (toPicture + 1) >= aImages.Length ? 0 : (toPicture + 1);
			mat.SetTexture ("_MainTex1", (Texture)aImages [toPicture]);
			mat.SetTexture ("_Gradient", (Texture)aDissolver[shader]);
		} 
		else if (fader == 1) {
			direction *= -1;
			toPicture = (toPicture + 1) >= aImages.Length ? 0 : (toPicture + 1);
			mat.SetTexture ("_MainTex", (Texture)aImages [toPicture]);
			mat.SetTexture ("_Gradient", (Texture)aDissolver[shader]);
		} 
		else {
			mat.SetFloat ("_Blend", fader);
		}
	}

	
	public IEnumerator Wait()
	{
		mat.SetFloat("_Blend", fader);
		wait = true;
		yield return new WaitForSeconds(3);
		wait = false;
	}
	
	
}
