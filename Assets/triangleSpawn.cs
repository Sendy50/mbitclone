using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class triangleSpawn : MonoBehaviour
{

    public GameObject triangle;

    private void Start()
    {
        InvokeRepeating("OutputTime", 0f, 0.3f);
    }

    // Start is called before the first frame update
    void OutputTime()
    {
        // yield return new WaitForSeconds(0.2f);
        Instantiate(triangle,transform.position,transform.rotation);
    }
    
    
    
}
