﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayPausePanel : MonoBehaviour
{
    public static PlayPausePanel Instance;

    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        AudioManager.Instance.OnPlayerPlayPause += MusicPlaying;
    }

    public void OnPlayBtnClick()
    {
        AudioManager.Instance.ResumeAudio();
    }
    
    public void OnPlayBtn1Click()
    {
        if (AudioManager.Instance.isPlaying)
            AudioManager.Instance.StopAudio();
        else
            AudioManager.Instance.ResumeAudio();
    }

    private void MusicPlaying(bool play)
    {
        // if (PreviewScreenManager.Instance.CreateDialog.activeInHierarchy && !play)
        // {
        //     AudioManager.Instance.ResumeAudio();
        //     return;
        // }
        
        if (play)
        {
            transform.GetChild(0).GetChild(0).gameObject.SetActive(false);
            GetComponent<Image>().color = new Color(0, 0, 0, 0);
        }
        else
        {
            transform.GetChild(0).GetChild(0).gameObject.SetActive(true);
            GetComponent<Image>().color = new Color(0, 0, 0, 0.4f);
        }
    }


    private void OnDisable()
    {
        AudioManager.Instance.OnPlayerPlayPause -= MusicPlaying;
    }
}