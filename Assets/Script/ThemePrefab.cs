﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Particle", menuName = "Create Particle")]
public class ThemePrefab : ScriptableObject
{
    public int UniqueID;
    
    public GameObject CatPrefab;

    public Sprite CatIcon;

    public string CatName;

    public bool _UnlockDefault;

    public bool _IsUnlocked;
    
    
}