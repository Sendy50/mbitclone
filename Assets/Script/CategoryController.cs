﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class CategoryController : MonoBehaviour
{
    public enum Category
    {
        Favorite,
        Special,
        Birthday,
        Love,
        Neon,
        God,
        Baby,
        Wish,
        Sports,
        Travels,
        Festival,
        Patrioric
    };
    
    public String jsonString;

    private ParticalData ParticalsData;

    public bool Favorite;
    public GameObject CategoryPrefab;
    public GameObject CategoryContent;
    public GameObject SubCategoryPanel;

    private List<GameObject> AllCatItem;
    public static CategoryController Instance;
    // private ResponseParticles responseParticles;

    private void Awake()
    {
        Instance = this;
    }

    public IEnumerator Start()
    {
        // PlugInTest.GetParticalData();
        // Debug.LogError("ParticalsData : "+ParticalsData.getParticalDetails().Count);
        // yield return new WaitUntil(() => GameManager.Instance.particleDeta!= null);
        // ParticalsData = GameManager.Instance.particleDeta; //JsonLoader.Instance.ParticalsData;

        string filePath = Utils.JsonPath + Utils.JsonFileName;
        Debug.LogError("Json load Path :"+filePath);
        
        if (File.Exists(filePath))
        {
            string dataAsJson = File.ReadAllText(filePath);
          
            
            
            // groupByPriceMap = responseParticles.getApp().stream().collect(Collectors.groupingBy(TemplateItem::getTemplateCatName));
            // strings = new ArrayList<>(groupByPriceMap.keySet());
            
            ParticalsData = JsonUtility.FromJson<ParticalData>(dataAsJson);
            GameManager.Instance.particleDeta = ParticalsData;
            Debug.LogError("loaded json data : "+ParticalsData.toString());
            Debug.LogError("loaded json partical size : "+ParticalsData.getParticalDetails().Count);
        }
        else
        {
            Debug.LogError("Cannot load json data");
            string filePath1 = Application.streamingAssetsPath + Utils.JsonFileName;
            string dataAsJson = File.ReadAllText(filePath1);
            Debug.LogError("load json data : "+dataAsJson);
            
            // responseParticles = JsonUtility.FromJson<ResponseParticles>(filePath1);
            //
            // var usersGroupedByCategoryName = responseParticles.app.GroupBy(user => user.ParticleCatName);
            //
            // // Debug.LogError("@@@@@@@" +usersGroupedByCategoryName.);
            // foreach (var items in usersGroupedByCategoryName)
            // {
            //     Debug.LogError("@@@@@@@" +items.Key+" -- ");
            //     foreach(var user in items)    
            //         Console.WriteLine("* " + user.ThemeName);
            // }
            
            ParticalsData = JsonUtility.FromJson<ParticalData>(dataAsJson);
            GameManager.Instance.particleDeta = ParticalsData;
            Debug.LogError("loaded json data : "+ParticalsData.toString());
            Debug.LogError("loaded json partical size : "+ParticalsData.getParticalDetails().Count);
            
//    Anniversary
//    Festivals
//    BabyShower
//    Wish
//    Travel
//    God
//    Summer
//    Travel
//    Slogan

        } 
        
        Debug.LogError("ParticalsData : "+ParticalsData.getParticalDetails().Count);
        
        AllCatItem = new List<GameObject>();
        
        foreach (ParticalDetailsItem catData in ParticalsData.getParticalDetails())
        {
            // if(catData._category == Category.Favorite && Favorite)
            GameObject catObject = Instantiate(CategoryPrefab,CategoryContent.transform).gameObject;
            catObject.transform.localScale = Vector3.one;
            catObject.GetComponent<CategoryItemController>().SetUp(catData);
            AllCatItem.Add(catObject);
        }

        yield return null;
    }
    
    
    public void OnCategoryItemClick(ParticalDetailsItem catDataCategory)
    {
        SubCategoryPanel.SetActive(true);
        SubCategoryController.Instance.ShowSubCategory(catDataCategory);
    }
    
    
    public void OnBackButtonClick()
    {
        // Debug.LogError("Back btn Click");
        SubCategoryPanel.SetActive(false);
        SubCategoryController.Instance.ClearChildren(SubCategoryController.Instance.SubCategoryContent);
    }
    
    
}


