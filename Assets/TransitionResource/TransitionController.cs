﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransitionController : MonoBehaviour
{
    // public GameObject ThemePrefab;
    public GameObject TransitionContent;
    public static TransitionController Instance;
    public TransitionData[] Transitions;
    public GameObject[] AllTransitionPrefab;

    private void Awake()
    {
        Instance = this;
    }


    [Serializable]
    public class TransitionData
    {
        public int Index;
        public String ThemeName;
        public bool ThemeIsLock;

    }

    public void ClearSelection()
    {
        foreach (GameObject child in AllTransitionPrefab)
        {
            child.GetComponent<TransitionItemController>()
                .Border.gameObject.SetActive(false);
           
            // child.GetComponent<TransitionItemController>().TransitionScript.gameObject.SetActive(false);
        }

        // StartCoroutine(UpdateAlltransition());
       
    }

    // private IEnumerator UpdateAlltransition()
    // {
    //     yield return new WaitForSeconds(1);
    //     foreach (GameObject child in AllTransitionPrefab)
    //     {
    //         child.GetComponent<TransitionItemController>().TransitionScript.gameObject.SetActive(true);
    //     }
    // }

    public void OnTransitionItemClick(int index)
    {
        if (index == GameManager.Instance.SelectedTransitionIndex)
        {
            return;
        }
        //Set Theme to MainRawImage Code Here
        Debug.LogError("Selected Theme : " + Transitions[index].ThemeName);
        GameManager.Instance.SelectedTransitionIndex = index;
        ClearSelection();
        AllTransitionPrefab[GameManager.Instance.SelectedTransitionIndex].GetComponent<TransitionItemController>()
            .Border.gameObject.SetActive(true);
        TransitionManager.Instance.ApplyTheme(index);
        // PlayPausePanel.Instance.OnPlayBtnClick();
    }


    // Start is called before the first frame update
    void Start()
    {
        AllTransitionPrefab = new GameObject[TransitionContent.transform.childCount];
        for (int i = 0; i < TransitionContent.transform.childCount; i++)
        {
            AllTransitionPrefab[i] = TransitionContent.transform.GetChild(i).gameObject;
            TransitionContent.transform.GetChild(i).GetComponent<TransitionItemController>().SetUp(Transitions[i]);
        }

        // if(catData._category == Category.Favorite && Favorite)
        // GameObject catObject = Instantiate(ThemePrefab,TransitionContent.transform).gameObject;
        // catObject.transform.localScale = Vector3.one;
    }
}