﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioSyncHeight : AudioSyncer
{
   	
    private IEnumerator MoveToScale(Vector2 _target)
    {
        RectTransform rt = (RectTransform)transform;
        Vector2 _curr =  rt.sizeDelta;
        Vector2 _initial = _curr;
        float _timer = 0;
		

        while (_curr != _target)
        {
            _curr = Vector2.Lerp(_initial, _target, _timer / timeToBeat);
            _timer += Time.deltaTime;
            // Debug.LogError("Move to scale : "+_curr);
            rt.sizeDelta = _curr;

            yield return null;
        }

        m_isBeat = false;
    }

    public override void OnUpdate()
    {
        base.OnUpdate();

        if (m_isBeat) return;

        RectTransform rt = (RectTransform)transform;
        rt.sizeDelta = Vector2.Lerp(transform.localScale, restSize, restSmoothTime * Time.deltaTime);
        rt.sizeDelta = new Vector2(57,100f);
    }

    public override void OnBeat()
    {
        base.OnBeat();

        StopCoroutine("MoveToScale");
        StartCoroutine("MoveToScale", beatSize);
    }

    public Vector2 beatSize;
    public Vector2 restSize;
}