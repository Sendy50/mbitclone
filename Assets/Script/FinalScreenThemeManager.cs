﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalScreenThemeManager : MonoBehaviour
{
    
   public GameObject BlinkerTheme;
   public GameObject BlinkerZoomTheme;
   public GameObject ZoomTheme;
   public GameObject TwistTheme;
   public GameObject WaveTheme;
   public GameObject Puzzle1Theme;
   public GameObject EqualizerTheme;
   public GameObject DancingOvelTheme;
   public GameObject BitSquareTheme;
  
   
   public void OffAllEffect()
   {
       BlinkerTheme.SetActive(false);
       BlinkerZoomTheme.SetActive(false);
       ZoomTheme.SetActive(false);
       TwistTheme.SetActive(false);
       WaveTheme.SetActive(false);
       Puzzle1Theme.SetActive(false);
       EqualizerTheme.SetActive(false);
       DancingOvelTheme.SetActive(false);
       BitSquareTheme.SetActive(false);
   }

    void Start()
    {
        OffAllEffect();
        if (GameManager.Instance.SelectedImages.Length == 1)
        {
            ApplyTheme(GameManager.Instance.SelectedThemeIndex);
        }
    }

    public void ApplyTheme(int Index)
    {
        OffAllEffect();
        switch (Index)
        {
            case 0:
                BlinkerTheme.SetActive(true);
                BlinkerTheme.transform.GetChild(0).GetComponent<Theme_Blinker>().Applytheme();
                break;
            case 1:
                BlinkerZoomTheme.SetActive(true);
                BlinkerZoomTheme.transform.GetChild(0).GetComponent<Theme_BlinkerZoom>().Applytheme();
                break;
            case 2:
                ZoomTheme.SetActive(true);
                ZoomTheme.transform.GetChild(0).GetComponent<Theme_Zoom>().Applytheme();
                break;
            case 3:
                TwistTheme.SetActive(true);
                TwistTheme.transform.GetChild(0).GetComponent<Theme_Twist>().Applytheme();
                break;
            case 4:
                WaveTheme.SetActive(true);
                WaveTheme.transform.GetChild(0).GetComponent<Theme_Wave>().Applytheme();
                break;
            case 5:
                Puzzle1Theme.SetActive(true);
                Puzzle1Theme.GetComponent<Theme_Puzzle1>().Applytheme();
                break;
            case 6:
                EqualizerTheme.SetActive(true);
                EqualizerTheme.GetComponent<Theme_Equalizer>().Applytheme();
                break;
            case 7:
                DancingOvelTheme.SetActive(true);
                DancingOvelTheme.GetComponent<Theme_DancingOval>().Applytheme();
                break;
            case 8:
                BitSquareTheme.SetActive(true);
                BitSquareTheme.GetComponent<Theme_BitSquare>().Applytheme();
                break;
            
        }
    }
    
}
