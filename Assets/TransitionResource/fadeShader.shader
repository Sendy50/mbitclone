﻿Shader "gemmine/transition/fadeShader"
{
    Properties
    {
        _MainTex ("Base (RGB)", 2D) = "white" {}
        _MainTex2 ("Base (RGB)", 2D) = "white" {}
        _Blend ( "Blend", Range ( 0, 1 ) ) = 0
    }
    SubShader
    {
        Tags
        {
//            "RenderType"="Opaque"
            "Queue" = "Transparent"
        }
        LOD 200

        CGPROGRAM
        #pragma surface surf Lambert

        sampler2D _MainTex;
        sampler2D _MainTex2;
        float _Blend;

        struct Input
        {
            float2 uv_MainTex;
            float2 uv_MainTex2;
        };

        void surf(Input IN, inout SurfaceOutput o)
        {
            half4 c1 = tex2D(_MainTex, IN.uv_MainTex);
            half4 c2 = tex2D(_MainTex2, IN.uv_MainTex);
            o.Albedo = lerp(c1, c2, _Blend);
            o.Alpha = c1.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}