﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NatCorder;
using NatCorder.Inputs;
using NatCorder.Clocks;
using System.IO;
using System;

public class UiManager : MonoBehaviour
{
    public GameObject Audio;
    private float CurrentAudioLength;


    [Header("Recording")]
    public int videoWidth = Screen.width;
    public int videoHeight = Screen.height;
    public bool recordMicrophone;

    private IMediaRecorder videoRecorder;
    private CameraInput cameraInput;
    private AudioInput audioInput;
    private AudioSource microphoneSource;

    private void Start()
    {
        // Start microphone
      //  microphoneSource = Audio.GetComponent<AudioSource>();
       // microphoneSource.mute =
       // microphoneSource.loop = true;
        //microphoneSource.bypassEffects =
        //microphoneSource.bypassListenerEffects = false;
       // microphoneSource.clip = Microphone.Start(null, true, 10, AudioSettings.outputSampleRate);
       // yield return new WaitUntil(() => Microphone.GetPosition(null) > 0);
       // microphoneSource.Play();
    }

    public void StartVideo() 
    {
       
        Audio.GetComponent<AudioSource>().Play();
        //recordManager.StartRecord();
        StartRecording();
    }

    public void SaveVid()
    {
        //recordManager.StopRecord();
        StopRecording();
    }

    private void Update()
    {
        if (Audio.GetComponent<AudioSource>().isPlaying &&  CurrentAudioLength <  Audio.GetComponent<AudioSource>().clip.length)
        {
            CurrentAudioLength = Audio.GetComponent<AudioSource>().time;
        }
        else
        {
            Audio.GetComponent<AudioSource>().Stop();
           // SaveVid();
        }
    }


    public void StartRecording()
    {

       

        // Start recording
        var frameRate = 30;
        var sampleRate = recordMicrophone ? AudioSettings.outputSampleRate : 0;
        var channelCount = recordMicrophone ? (int)AudioSettings.speakerMode : 0;
        var recordingClock = new RealtimeClock();
        videoRecorder = new MP4Recorder(
            videoWidth,
            videoHeight,
            frameRate,
            sampleRate,
            channelCount,
            recordingPath => {
                Debug.LogError($"Saved recording to: {recordingPath}");
                // var prefix = Application.platform == RuntimePlatform.IPhonePlayer ? "file://" : "";
                // Handheld.PlayFullScreenMovie($"{prefix}{recordingPath}");
               
                string filename = Path.GetFileName(recordingPath);
                NativeGallery.SaveVideoToGallery(recordingPath, "MBitClone", filename, (path) => Debug.Log("Media save result: " + path + " "));
                DeleteTempFile(recordingPath, filename);
            }
        );
        // Create recording inputs
        cameraInput = new CameraInput(videoRecorder, recordingClock, Camera.main);
        audioInput = recordMicrophone ? new AudioInput(videoRecorder, recordingClock, Audio.GetComponent<AudioSource>(), false) : null;
        // Unmute microphone
        Audio.GetComponent<AudioSource>().mute = audioInput == null;
    }

    private void DeleteTempFile(string filePath, string filename)
    {
        if (!File.Exists(filePath))
        {
            Debug.LogError(filename + " file exists, deleting..."); //Debug.Log( fileName + " file exists, deleting..." );

            File.Delete(filePath);
        }
    }

    public void StopRecording()
    {
        // Stop recording
        audioInput?.Dispose();
        cameraInput?.Dispose();
        videoRecorder?.Dispose();
        // Mute microphone
        Audio.GetComponent<AudioSource>().mute = true;


    }
}
